<?php
ini_set('date.timezone', 'America/Lima');
$time1 = date('H:i:s', time());
$time2 = date('Y-m-d, H:i:s', time());
//echo date("g:i a", strtotime($time1));
print'<br>';
//echo $time1;
//print'<br>';
//echo $time2;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>EPG - Asistencia</title>
        <link rel="shortcut icon" href="../public/img/favicon.ico">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="required/bootstrap.min.css">
        <link rel="stylesheet" href="required/font-awesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="required/AdminLTE.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.css">

    </head>

    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <img src="required/logo.png">
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body" style="border-radius: 5px;">
                <div class="box box-primary" style="border-top-color: #8f1656 !important;">
                    <div class="login-logo">
                        <a href="#"><b>Asistencia-</b>EPG</a>
                    </div>  
                    <form method="post" id="frmAcceso" name="frmAcceso">
                        <div class="form-group has-feedback" style="text-align: center;">
                            <label id="fechaso" style="font-family: arial;"></label> <br>
                            <label > <h1><table id="reloj" border='0'>
                                        <tr>
                                            <td><h1><div id='hora' style="font-family: fantasy; font-size: 50;"></div></h1></td>
                                            <td><h1><div>:</div></h1></td>
                                            <td><h1><div id='minuto' style="font-family: fantasy; font-size: 50;"></div></h1></td>
                                            <td><div>:</div></td>
                                            <td><div id='segundo'></div></td>
                                        </tr>
                                    </table></h1></label>
                        </div>
                        <div id="hora"></div>
                        <input type="hidden" name="relojo" id="relojo">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="DNI / CE / Pasaporte" name="nrodocumento" id="nrodocumento" required maxlength="12">
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" style="background-color: #8f1656 !important; border-color: #8f1656 !important; border-radius: 5px;">Registrar Asistencia </span></button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery 2.2.3 -->
    <script src="required/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="required/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
    <script src="required/bootbox.min.js"></script>
    <script src="vistas/scripts/login.js"></script>
    <!-- <script>
        function hora() {
                $.ajax({
                    // type: 'GET',
                    // url: 'hora.php',
                    // success: function ($hora) {
                    //     $("#hora").html($hora);
                    //     console.log("get");
                    //     //setTimeout('hora()', 10000);
                    // }
                });
            }
            hora();
            //setTimeout('hora()', 10000);
            
        $('document').ready(function () {
            
        });
    </script> -->
</body>
</html>
