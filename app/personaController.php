<?php

require_once "./personaModel.php";

$persona=new Persona();

$person_nombre = isset($_POST["person_nombre"])? limpiarCadena($_POST["person_nombre"]):"";
$person_code = isset($_POST["person_code"])? limpiarCadena($_POST["person_code"]):"";
$person_unidad = isset($_POST["person_unidad"])? limpiarCadena($_POST["person_unidad"]):"";
$person_email = isset($_POST["person_email"])? limpiarCadena($_POST["person_email"]):"";
$person_lugar = isset($_POST["person_lugar"])? limpiarCadena($_POST["person_lugar"]):"";
$person_habit = isset($_POST["person_habit"])? limpiarCadena($_POST["person_habit"]):"";
$person_uso = isset($_POST["person_uso"])? limpiarCadena($_POST["person_uso"]):"";
$person_fechin = isset($_POST["person_fechin"])? limpiarCadena($_POST["person_fechin"]):"";
$person_horain = isset($_POST["person_horain"])? limpiarCadena($_POST["person_horain"]):"";
$person_fechout = isset($_POST["person_fechout"])? limpiarCadena($_POST["person_fechout"]):"";
$person_horaout = isset($_POST["person_horaout"])? limpiarCadena($_POST["person_horaout"]):"";
$person_celular = isset($_POST["person_celular"])? limpiarCadena($_POST["person_celular"]):"";
$person_eupeu = isset($_POST["person_eupeu"])? limpiarCadena($_POST["person_eupeu"]):"";

$comentario = isset($_POST["comentario"])? limpiarCadena($_POST["comentario"]):"";
$ruc = isset($_POST["ruc"])? limpiarCadena($_POST["ruc"]):"";
$direccion = isset($_POST["direccion"])? limpiarCadena($_POST["direccion"]):"";

$razon = isset($_POST["razonsoc"])? limpiarCadena($_POST["razonsoc"]):"";
if ($razon=='') {
    $tipocomp = limpiarCadena("Boleta");
    $razonsoc = isset($_POST["razonsoc2"])? limpiarCadena($_POST["razonsoc2"]):"";
}  else {
    $tipocomp = limpiarCadena("Factura");
    $razonsoc = isset($_POST["razonsoc"])? limpiarCadena($_POST["razonsoc"]):"";
 }

$person_file = isset($_POST["person_file"])? limpiarCadena($_POST["person_file"]):"";
        
//$imagen=isset($_POST["imagen"])? limpiarCadena($_POST["imagen"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
        
		if (!file_exists($_FILES['person_file']['tmp_name']) || !is_uploaded_file($_FILES['person_file']['tmp_name']))
		{
			$person_file="";
		}
		else 
		{
			$ext = explode(".", $_FILES["person_file"]["name"]);
			if ($_FILES['person_file']['type'] != "application/x-msdownload" )
			{
				$person_file = round(microtime(true)) . '.' . end($ext);
				move_uploaded_file($_FILES["person_file"]["tmp_name"], "picture/" . $person_file);
            }
		}
		if (empty($idarticulo)){
			$rspta=$persona->insertar($person_nombre,$person_code, $person_unidad, $person_email,$person_lugar, $person_habit, $person_uso, $person_fechin, $person_horain, $person_fechout, $person_horaout, $person_file, $comentario, $tipocomp, $razonsoc, $ruc, $direccion, $person_celular, $person_eupeu);
			echo $rspta ? "ok" : "fail";
		}
		else {
			$rspta=$persona->editar($idarticulo,$idcategoria,$codigo,$nombre,$stock,$descripcion,$imagen);
			echo $rspta ? "Artículo actualizado" : "Artículo no se pudo actualizar";
		}
	break;
}
?>