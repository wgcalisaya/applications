<?php
include "app/connection.php";

if (isset($_POST["button_add"])) {
    $nombre = time() . $_FILES['person_file']['name'];
    $tipevent = "1";

    $qry = mysqli_query($con, "insert into persona (person_nombre,person_code, person_unidad, person_email,person_lugar, person_habit, person_uso, person_fechin, person_horain, person_fechout, person_horaout, person_file, tipevent ) values(
	    	'" . $_POST["person_nombre"] . "
	    	','" . $_POST["person_code"] . "
	    	','" . $_POST["person_unidad"] . "
	    	','" . $_POST["person_email"] . "
	    	','" . $_POST["person_lugar"] . "
	    	','" . $_POST["person_habit"] . "
	    	','" . $_POST["person_uso"] . "
	    	','" . $_POST["person_fechin"] . "
	    	','" . $_POST["person_horain"] . "
	    	','" . $_POST["person_fechout"] . "
	    	','" . $_POST["person_horaout"] . "
	    	','" . $nombre . "
                ','" . $tipevent . "
    	')") or die("No se puede ejecutar la peticion.");
    if ($qry) {
        $target_dir = "app/picture/";
        $target_file = $target_dir . basename($nombre);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES["person_file"]["tmp_name"], $target_file)) {
            //echo "se envio correctamente!";
            echo '<script> alert ("Tus datos se enviaron correctamente.");</script>';
        } else {
            echo "Algo salio mal!";
        }
    }
}
?>



<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="en">
    <!--<![endif]-->
    <script src="js/include.js"></script>	

    <head>
        <meta charset="utf-8">
        <title>Escuela de Posgrado | UPeU</title>
        <meta name="description" content="Profesionales alatamente confiables">
        <meta name="author" content="htmlcoder.me">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Fontello CSS -->
        <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

        <!-- Plugins -->
        <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
        <link href="css/animations.css" rel="stylesheet">
        <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="plugins/hover/hover-min.css" rel="stylesheet">		
        <link href="plugins/morphext/morphext.css" rel="stylesheet">
        <link rel="stylesheet" href="admin/css/sweetalert.css">

        <!-- The Project's core CSS file -->
        <!-- Use css/rtl_style.css for RTL version -->
        <link href="css/style.css" rel="stylesheet" >
        <!-- The Project's Typography CSS file, includes used fonts -->
        <!-- Used font for body: Roboto -->
        <!-- Used font for headings: Raleway -->
        <!-- Use css/rtl_typography-default.css for RTL version -->
        <link href="css/typography-default.css" rel="stylesheet" >
        <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
        <link href="css/skins/gold.css" rel="stylesheet">


        <!-- Custom css --> 
        <link href="css/custom.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <!-- "gradient-background-header": applies gradient background to header -->
    <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
    <body class="no-trans front-page   page-loader-2">

        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">

            <!-- header-container start -->
            <div class="header-container">
                <!-- ================ --> 
                <script>
                    w3IncludeHTML();
                </script>
                <!-- header end -->
            </div>
            <!-- header-container end -->

            <!-- banner start -->
            <!-- ================ -->
            <div class="banner banner-big-height dark-translucent-bg padding-bottom-clear" style="background-image:url('images/hotel-banner.jpg');background-position: 50% 32%;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 text-center col-md-offset-2 pv-20">
                            <h1 class="title">Reservar residencia</h1>
                            <div class="separator mt-10"></div>
                        </div>
                    </div>
                </div>
                <!-- section start -->
                <!-- ================ -->
                <div class="dark-translucent-bg section">
                    <div class="container">
                        <!-- filters start -->

                        <!-- INICIO -->
                        <div class="sorting-filters text-center mb-20">
                            <form name="formulario" id="formulario" class="form-inline">

                                <div class="form-group">
                                    <label style="text-align:center">Nombre Completo</label>
                                    <input type="text" name="person_nombre" id="person_nombre"  placeholder="Nombre completo"  size="27" maxlength="100" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Codigo estudiante</label>
                                    <input type="text" name="person_code" id="person_code"  placeholder="cod estudiante"  size="10" maxlength="10" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Seleccione Unidad</label>
                                    <select class="form-control" name="person_unidad" id="person_unidad" placeholder="" required>
                                        <option value="null">Seleccione Unidad</option>
                                        <option value="Posgrado Ciencias Empresariales">Posgrado Ciencias Empresariales</option>
                                        <option value="Posgrado Ciencias Humanas y Educación">Posgrado Ciencias Humanas y Educación</option>
                                        <option value="Posgrado de Ingeniería y Arquitectur">Posgrado de Ingeniería y Arquitectura</option>
                                        <option value="Posgrado Teología">Posgrado Teología</option>		  
                                    </select>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Email</label>
                                    <input type="text" name="person_email" id="person_email" placeholder="correo@mail.com"  size="50" maxlength="100" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Lugar de procedencia</label>
                                    <input type="text" name="person_lugar" id="person_lugar" placeholder="Lima-Peru"  size="30" maxlength="100" class="form-control" required>
                                </div>


                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Tipo de servicio*</label>
                                    <select class="form-control" name="person_habit" id="person_habit" placeholder="" required>
                                        <option value="Normal (residencia, alimentación y lavandería)">Normal (residencia, alimentación y lavandería)</option>
                                        <option value="Económico 1 (residencia  y alimentación)">Económico 1 (residencia  y alimentación)</option>
                                        <option value="Económico 2(residencia y lavandería)">Económico 2(residencia y lavandería)</option>
                                        <option value="Económico 3 (sólo residencia)">Económico 3 (sólo residencia)</option>
                                    </select>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Fecha de Ingreso</label>
                                    <input type="date" class="form-control" name="person_fechin" id="person_fechin" required>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Hora de Ingreso</label>
                                    <input name="person_horain" id="person_horain" type="text" size="7" maxlength="10" class="form-control" placeholder="11:30pm" required>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Fecha de salida</label>
                                    <input type="date" class="form-control" name="person_fechout" id="person_fechout" required>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Hora de Salida</label>
                                    <input name="person_horaout" id="person_horaout" type="text" size="7" maxlength="100" class="form-control"  placeholder="6:00am" required>
                                </div>
                                <br>



                                <div class="form-group">
                                    <label style="text-align:center">Seleccion su boucher</label>
                                    <input type="file" name="person_file" id="person_file" class="form-control" required>
                                    <input name="boton" type="hidden" value="guardar">
                                </div>

                                <div class="separator mt-10"></div>

                                
                            </form>
                            <button type="button" id="btnGuardar" onclick="guardar();" name="button_add" style="width:15%"  class="btn btn-lg btn-gray-transparent btn-animated margin-clear" >Enviar<i class="fa fa-check"></i></button>

                            <script type="text/javascript">
                                function guardar() {
                                    var nombre = $('#person_nombre').val();
                                    var codigo = $('#person_code').val();
                                    if (nombre == "" || codigo =="") {
                                        alert(" >> Uno de los campos esta vacio <<");
                                    }else{
                                        
                                        var formData = new FormData($("#formulario")[0]);
                                    $.ajax({
                                        url: 'app/personaController.php?op=guardaryeditar',
                                        type: 'POST',
                                        data: formData,
                                        cache: false,
                                        processData: false,
                                        contentType: false,
                                    }).done(function (resp) {
                                        if (resp == 'ok') {
                                            swal('Gracias', 'Sus datos fueron registrados correctamente', 'success');
                                            
                                        }
                                        $('#person_nombre').val("");

                                        
                                        /*if(resp==='exito'){
                                         $('#exito').show();
                                         lista_libros('',1);
                                         $("#formLibro")[0].reset();
                                         }
                                         else{
                                         alert(resp);
                                         }*/
                                    });
                                    }
                                }
                            </script>
                            <script src="admin/js/sweetalert.min.js"></script>
                        </div>
                        <!-- FIN -->
                        <!-- filters end -->
                    </div>
                </div>
                <!-- section end -->
            </div>



            <section class="section clearfix">
                <div class="container">
                </div>
            </section>

            <!-- section end -->

            <!-- footer top start -->
            <!-- ================ -->
            <script>
                                w3IncludeHTML();
            </script>
            <!-- footer end -->

        </div>

    </div>
    <!-- footer top end -->		
    <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
    <!-- ================ -->


    <!-- .footer start -->
    <!-- ================ -->


    <!-- footer end -->

    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->

    <script type="text/javascript" src="plugins/jquery.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- jQuery Revolution Slider  -->
    <script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Isotope javascript -->
    <script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Morphext -->
    <script type="text/javascript" src="plugins/morphext/morphext.min.js"></script>
    <!-- Google Maps javascript -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=your_google_map_key"></script>
    <script type="text/javascript" src="js/google.map.config.js"></script>
    <!-- Background Video -->
    <script src="plugins/vide/jquery.vide.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Pace javascript -->
    <script type="text/javascript" src="plugins/pace/pace.min.js"></script>
    <!-- SmoothScroll javascript -->
    <script type="text/javascript" src="plugins/jquery.browser.js"></script>
    <script type="text/javascript" src="plugins/SmoothScroll.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>



</body>
</html>
