
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <!--<![endif]-->
    <head>
        <script src="https://www.w3schools.com/lib/w3data.js"></script>
        <meta charset="utf-8">
        <title>Escuela de Posgrado | UPeU</title>
        <meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
        <meta name="author" content="htmlcoder.me">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="../images/favicon.ico">

        <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="../fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Fontello CSS -->
        <link href="../fonts/fontello/css/fontello.css" rel="stylesheet">

        <!-- Plugins -->
        <link href="../plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="../css/animations.css" rel="stylesheet">
        <link href="../plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../plugins/hover/hover-min.css" rel="stylesheet">		

        <!-- The Project's core CSS file -->
        <!-- Use css/rtl_style.css for RTL version -->
        <link href="../css/style.css" rel="stylesheet" >
        <!-- The Project's Typography CSS file, includes used fonts -->
        <!-- Used font for body: Roboto -->
        <!-- Used font for headings: Raleway -->
        <!-- Use css/rtl_typography-default.css for RTL version -->
        <link href="../css/typography-default.css" rel="stylesheet" >
        <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
        <link href="../css/skins/light_blue.css" rel="stylesheet">

        <link rel="stylesheet" href="../css/popup.css">

        <!-- Custom css --> 
        <link href="../css/custom.css" rel="stylesheet">
        
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
          ga('create', 'UA-104256868-20', 'auto');
          ga('send', 'pageview');
    
        </script>
    </head>

    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <!-- "gradient-background-header": applies gradient background to header -->
    <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
    <body class="no-trans    ">
        
                                        
                                    

        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">

            <!-- header-container start -->

            <div w3-include-html="../header2.html"></div>

            <script>
                w3IncludeHTML();
            </script>

            <!-- header-container end -->

            <!-- banner start -->
            <!-- ================ -->
            <div class="banner default-translucent-bg" style="background-image:url('../images/upgpsico/dificultadesa.jpg');background-position:50% 0;">
                <!-- breadcrumb start -->
                <!-- ================ -->
                <div class="breadcrumb-container">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="../index.html">Inicio</a></li>
                            <li class="active">Curso|Evaluación e Intervención en Dificultades del Aprendizaje</li>
                            <a href="../Cursos/dificulapren.html"></a>
                        </ol>
                    </div>
                </div>
                <!-- breadcrumb end -->
                <div class="container"  >
                    
                    
                    <div class="row">
                        <div class="col-md-7 text-center col-md-offset-0 pv-20"  >

                            <div class="col-md-12 text-center col-md-offset-0 pv-20"     >
                                <h1><strong>Contáctenos</strong></h1>
                                <form role="form" id="formulario">

                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group has-success has-feedback">
                                            <input type="text" class="form-control" placeholder="Nombres - Apellidos*" id="nombres">
                                            <i class="fa fa-user form-control-feedback"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group has-success has-feedback">
                                            <input type="text" class="form-control" placeholder="Correo*" id="email">
                                            <i class="fa fa-envelope-o form-control-feedback"></i>
                                        </div>
                                    </div>
                                    <select id="carrera" name="carrera" onchange="change_country(this.value)" class="frm-field required img-responsive" style="display:none">
                                        <option value="44-Maestría en Ciencias de la Familia: Terapía Familiar" selected="">Maestría en Ciencias de la Familia: Terapía Familiar</option>
                                    </select>
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group has-success has-feedback">
                                            <input type="text" class="form-control" placeholder="Celular*" id="telefono">
                                            <i class="fa fa-mobile-phone form-control-feedback"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group has-success has-feedback">
                                            <input type="text" class="form-control" placeholder="Codigo de alumno" id="mensaje">
                                            <i class="fa form-control-feedback"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                Importante:
                                                <p> <strong>Enviar voucher escaneado de pago al correo eventosposgrado@upeu.edu.pe</strong></p>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-md-offset-2">
                                        <button type="submit" class="button-enviar btn btn-lg btn-dark btn-animated margin-clear" id="button-enviar">Enviar<i class="fa fa-send"></i></button>
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {
                                        $(".button-enviar").click(function () {
                                            $("#button-enviar").attr("disabled", false);

                                            var nombres = $("#nombres").val();
                                            var email = $("#email").val();
                                            var telefono = $("#telefono").val();
                                            var carrera = $("#carrera").val();
                                            var mensaje = $("#mensaje").val();
                                            var politicas = document.getElementById("check").checked;
                                            if (nombres == "") {
                                                alert("Debes ingresar un nombre válido");
                                                $("#button-enviar").attr("disabled", false);
                                            } else {
                                                if (email == "") {
                                                    alert("Debes ingresar un correo válido");
                                                } else {
                                                    if (telefono == "") {
                                                        alert("Debes ingresar un telefono válido");
                                                    } else {
                                                        if (politicas == true) {
                                                            $("#formulario").html('<br><br><br> <div class="separator"></div> <div class="separator"></div> <h2><strong>Enviando Datos ...</strong></h2> <div class="separator"></div> <div class="separator"></div>');
                                                            var formData = {nombres: nombres, telefono: telefono, email: email, carrera: carrera, mensaje: mensaje};
                                                            $.ajax({
                                                                type: "POST",
                                                                data: formData,
                                                                url: "https://patmos.upeu.edu.pe/index/contactoWeb/posgrado",
                                                                success: function (respuesta) {
                                                                    var da = respuesta;
                                                                    if (da != "0") {
                                                                        $("#button-enviar").attr("disabled", false);
                                                                        $("#formulario").html('<br>     <div class="separator"></div> <div class="separator"></div> <h3><strong>Hola ' + da + ', te invitamos a participar del webinar gratuito este 10 de abril.</strong></h3> <div class="separator"></div> <div class="separator"></div>');
                                                                    } else {
                                                                        $("#button-enviar").attr("disabled", false);
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            alert("Debes aceptar los términos y condiciones para enviar tus datos")
                                                        }

                                                    }

                                                }

                                            }
                                        });
                                    });
                                </script>
                            </div>

                        </div>
                        <br><br><br><br>
                        <div class="col-md-5 text-center col-md-offset-0 pv-5"   >
                            <h1 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100" font style=" font-size:  40px;" >Evaluación e Intervención en Dificultades de Aprendizaje</h1>
                            <div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
                            <p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">Las dificultades del aprendizaje se presentan con mayor frecuencia en la infancia. La mayoría de las veces se diagnostican o no llegan a detectarse (Mateos, 2011). 
                            </p>
                            <p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">    El presente curso de evaluación e intervención en dificultades del aprendizaje, tiene por objetivo mostrar técnicas de evaluación para la detección e intervención de los problemas de aprendizaje relacionados con la lecto - escritura para el nivel inicial y primario.
                        </div>

                    </div>

                </div>
            </div>
            <!-- banner end -->

            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container">
                
<?php
include "../app/connection.php";

if (isset($_POST["addPsico"])) {
    $nombres = trim($_POST["nombres"]);
    $tipodoc = trim($_POST["dni"]) ;
    $nrodoc = trim($_POST["numerodoc"]) ;
    $nacionalidad = trim($_POST["nacionalidad"]);
    $fechanac = trim($_POST["fechanac"]);
    $person_email= trim($_POST["email"]);
    $celular = trim($_POST["celular"]);
    $egresado = trim($_POST["egresado"]);
    $tipevento = "2";
    $estadomail = "no";
	$nombrefile = time().$_FILES['boucher']['name'];
    
    $succer = "alert alert-success";
        
    //$qry = mysqli_query($con, "insert into persona (person_nombre) values ('" . $nombres . "')") or die("No se puede ejecutar la peticion.");

    $qry = mysqli_query($con, "insert into persona (person_nombre, tipodoc, nrodoc, nacionalidad, fechanac, person_email, celular, egresado, tipevent, estadomail, person_file) values (
	    	'" . $nombres . "
	    	','" . $tipodoc. "
	    	','" . $nrodoc . "
	    	','" . $nacionalidad. "
	    	','" . $fechanac . "
	    	','" . $person_email . "
	    	','" . $celular . "
	    	','" . $egresado . "
            ','" . $tipevento . "
            ','" . $estadomail."
	    	','" . $nombrefile."
    	')") or die("No se puede ejecutar la peticion.");
    if ($qry) {
        $target_dir = "../app/picture/";
        $target_file = $target_dir . basename($nombrefile);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES["boucher"]["tmp_name"], $target_file)) {
            //echo "<center><h1>se envio correctamente!</h1></center>";
            //echo '<div class="alert alert-success"><strong>En buena hora!</strong> Se envio correctamente</div>';
            //echo 'swal("Good job!", "You clicked the button!", "success")';
            echo '<script> alert ("Tus datos se enviaron correctamente.");</script>';
            
        } else {
        echo "Algo salio mal!";
        }
       }
      } 
?>

                <div class="container">
                    <div class="row">

                        <!-- main start -->
                        <!-- ================ -->
                        <div class="main col-md-12">

                            <!-- page-title start -->
                            <!-- ================ -->
                            <h1 class="page-title">Docentes</h1>
                            <div class="separator-2"></div>
                            <!-- page-title end -->
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="image-box team-member shadow mb-20" style="width:200px">
                                        <div class="overlay-container overlay-visible">
                                            <img src="../images/upgpsico/pilar.png" alt="">

                                            <div class="overlay-bottom">
                                                <p class="margin-clear">Pilar Razuri Valdivia</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-box team-member shadow mb-20" style="width:200px">
                                        <div class="overlay-container overlay-visible">
                                            <img src="../images/upgpsico/Liliana.jpg" alt="">

                                            <div class="overlay-bottom">
                                                <p class="margin-clear">Liliana Acosta Portilla</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="fb-root"></div>

                                <div class="col-lg-4 col-sm-5" style="border: ">
                                    <p align="justify">Psicóloga educativa colegiada con 22 años de experiencia y formación cognitivo-conductual. Magister en educación en la mención de dificultades de aprendizaje. Docente universitaria de la facultad de Psicología y tutora virtual de docentes de la PUCP.</p>
                                    <p style="text-align:left">&nbsp; </p>
                                    <p style="text-align:left">&nbsp; </p>

                                    <div class="separator-2"></div>
                                    <p align="justify">Psicóloga educativa colegiada, con más de 25 años de experiecia. Posee una Segunda Especialidad en Problemas de Aprendizaje. A ejercido su profesión como Psicóloga Prevac, Coprecad, Docente de Educación Superior.</p>

                                    <h3><strong>Módulos</strong></h3>
                                    <a class="btn btn-gray collapsed btn-animated" data-toggle="collapse" href="#collapseContent" aria-expanded="false" aria-controls="collapseContent">Aquí<i class="fa fa-plus"></i></a>

                                </div>


                                <div class="col-sm-3 col-lg-offset-1">
                                    <h3 class="title"><strong>Informes</strong></h3>
                                    <h5 class="">UPG Psicología</h5>
                                    <ul class="list-icons">
                                        <li><i class="fa fa-phone pr-10 text-default"></i> 051 + 989111196</li>
                                        <li><i class="fa fa-mobile pr-10 text-default"></i> 051 + 989249801</li>
                                        <li><i class="fa fa-phone pr-10 text-default"></i> 618 6315 anexo 2015</li>
                                        <li><i class="fa fa-envelope-o pr-10 text-default"></i>eventosposgrado@upeu.edu.pe</li>
                                        <li><strong>Horarios de Atención</strong></li>
                                        <li><i class="fa fa-clock-o pr-10 text-default"></i>Lunes a jueves 8:00am a 1:00pm y 3:00pm a 5:30pm</li>
                                        <li><i class="fa fa-map-marker  pr-10 text-default"></i>Carretera central km.19.5 Lima,chosica</li>
                                    </ul>
                                    <div class="col-md-8 col-md-offset-2">
                                        <button type="submit" class="button-enviar btn btn-lg btn-dark btn-animated margin-clear" data-toggle="modal" data-target="#MyModal">Inscríbete</button>
                                    </div>
                                    
                                    
                                    

                                    <div id="MyModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 style="color: White ;" >Registrate</h4>
                                                </div>
                                                <form role="form" id="formInscripcion" action='<?php echo $_SERVER["PHP_SELF"]; ?>' method="POST" enctype="multipart/form-data">
                                                    <div class="modal-body">
                                                        <div class="container-fluid">



                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Nombres:</label>
                                                                    <input type="text" class="form-control" name="nombres" placeholder="Nombres y Apellidos" id="nombres" maxlength="100" required>
                                                                    <i class="fa fa-user form-control-feedback"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Dni:</label>
                                                                    <input type="radio" name="dni" value="dni" id="dni" required="" />
                                                                    <label>Carnet Extrangeria:</label>
                                                                    <input type="radio" name="dni" value="extrangero" id="dni"/>
                                                                    <label>Pasaporte:</label>
                                                                    <input type="radio" name="dni" value="extrangero" id="dni"/>
                                                                    <input type="number" name="numerodoc" class="form-control" placeholder="00000000" id="numerodoc" maxlength="20" required>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                <label>Nacionalidad:</label>
                                                                <select class="form-control" name="nacionalidad" id="nacionalidad" required="">
                                                                    <option value="">- seleccione -</option>                      
                                                                    <option value="Alemania">Alemania</option>                      
                                                                    <option value="Antillas Neerlandesas">Antillas Neerlandesas</option>                      
                                                                    <option value="Arabia Saudita">Arabia Saudita</option>                      
                                                                    <option value="Argentina">Argentina</option>                      
                                                                    <option value="Armenia">Armenia</option>                      
                                                                    <option value="Aruba">Aruba</option>                      
                                                                    <option value="Australia">Australia</option>                      
                                                                    <option value="Austria">Austria</option>                      
                                                                    <option value="Azerbaijan">Azerbaijan</option>                      
                                                                    <option value="Bélgica">Bélgica</option>                      
                                                                    <option value="Bahamas">Bahamas</option>                      
                                                                    <option value="Bangladesh">Bangladesh</option>                      
                                                                    <option value="Barbados">Barbados</option>                      
                                                                    <option value="Belarús">Belarús</option>                      
                                                                    <option value="Belice">Belice</option>                      
                                                                    <option value="Benin">Benin</option>                      
                                                                    <option value="Bermuda">Bermuda</option>                      
                                                                    <option value="Bhutan">Bhutan</option>                      
                                                                    <option value="Bolivia">Bolivia</option>                      
                                                                    <option value="Bosnia And Herzegowina">Bosnia And Herzegowina</option>                      
                                                                    <option value="Botswana">Botswana</option>                      
                                                                    <option value="Brasil">Brasil</option>                      
                                                                    <option value="Bulgaria">Bulgaria</option>                      
                                                                    <option value="Burundi">Burundi</option>                      
                                                                    <option value="Cabo Verde">Cabo Verde</option>                      
                                                                    <option value="Camboya">Camboya</option>                      
                                                                    <option value="Camerún">Camerún</option>                      
                                                                    <option value="Canada">Canada</option>                      
                                                                    <option value="Chad">Chad</option>                      
                                                                    <option value="Chile">Chile</option>                      
                                                                    <option value="China">China</option>                      
                                                                    <option value="Chipre">Chipre</option>                      
                                                                    <option value="Ciudad del Vaticano">Ciudad del Vaticano</option>                      
                                                                    <option value="Cocos Keeling Islands">Cocos Keeling Islands</option>                      
                                                                    <option value="Colombia">Colombia</option>                      
                                                                    <option value="Comoras">Comoras</option>                      
                                                                    <option value="Congo">Congo</option>                      
                                                                    <option value="Costa Rica">Costa Rica</option>                      
                                                                    <option value="Costa de Marfil">Costa de Marfil</option>                      
                                                                    <option value="Croacia">Croacia</option>                      
                                                                    <option value="Cuba">Cuba</option>                      
                                                                    <option value="Dinamarca">Dinamarca</option>                      
                                                                    <option value="Dominica">Dominica</option>                      
                                                                    <option value="Dominican Republic">Dominican Republic</option>                      
                                                                    <option value="Ecuador">Ecuador</option>                      
                                                                    <option value="Egypt">Egypt</option>                      
                                                                    <option value="El Salvador">El Salvador</option>                      
                                                                    <option value="Emiratos Árabes Unidos">Emiratos Árabes Unidos</option>                      
                                                                    <option value="Eslovaquia">Eslovaquia</option>                      
                                                                    <option value="Eslovenia">Eslovenia</option>                      
                                                                    <option value="España">España</option>                      
                                                                    <option value="Estados Unidos">Estados Unidos</option>                      
                                                                    <option value="Estonia">Estonia</option>                      
                                                                    <option value="Etiopía">Etiopía</option>                      
                                                                    <option value="Federación Rusia">Federación Rusia</option>                      
                                                                    <option value="Filipinas">Filipinas</option>                      
                                                                    <option value="Finlandia">Finlandia</option>                      
                                                                    <option value="Francia">Francia</option>                      
                                                                    <option value="Francia, Metropolitana">Francia, Metropolitana</option>                      
                                                                    <option value="Georgia">Georgia</option>                      
                                                                    <option value="Ghana">Ghana</option>                      
                                                                    <option value="Gibraltar">Gibraltar</option>                      
                                                                    <option value="Granada">Granada</option>                      
                                                                    <option value="Grecia">Grecia</option>                      
                                                                    <option value="Groenlandia">Groenlandia</option>                      
                                                                    <option value="Guadalupe">Guadalupe</option>                      
                                                                    <option value="Guam">Guam</option>                      
                                                                    <option value="Guatemala">Guatemala</option>                      
                                                                    <option value="Guayana Francesa">Guayana Francesa</option>                      
                                                                    <option value="Guinea">Guinea</option>                      
                                                                    <option value="Guinea Ecuatorial">Guinea Ecuatorial</option>                      
                                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>                      
                                                                    <option value="Guyana">Guyana</option>                      
                                                                    <option value="Haiti">Haiti</option>                      
                                                                    <option value="Heard And Mc Donald Islands">Heard And Mc Donald Islands</option>                      
                                                                    <option value="Honduras">Honduras</option>                      
                                                                    <option value="Hong Kong">Hong Kong</option>                      
                                                                    <option value="Hungría">Hungría</option>                      
                                                                    <option value="India">India</option>                      
                                                                    <option value="Indonesia">Indonesia</option>                      
                                                                    <option value="Iran">Iran</option>                      
                                                                    <option value="Iraq">Iraq</option>                      
                                                                    <option value="Irlanda">Irlanda</option>                      
                                                                    <option value="Isla Bouvet">Isla Bouvet</option>                      
                                                                    <option value="Isla Christmas">Isla Christmas</option>                      
                                                                    <option value="Islandia">Islandia</option>                      
                                                                    <option value="Islas Caimán">Islas Caimán</option>                      
                                                                    <option value="Islas Cook">Islas Cook</option>                      
                                                                    <option value="Islas Feroe">Islas Feroe</option>                      
                                                                    <option value="Islas Marianas del Norte">Islas Marianas del Norte</option>                      
                                                                    <option value="Islas Marshall">Islas Marshall</option>                      
                                                                    <option value="Islas Norfolk">Islas Norfolk</option>                      
                                                                    <option value="Islas Pitcairn">Islas Pitcairn</option>                      
                                                                    <option value="Islas Salomón">Islas Salomón</option>                      
                                                                    <option value="Islas Turcas y Caicos">Islas Turcas y Caicos</option>                      
                                                                    <option value="Islas Vírgenes  (Britan">Islas Vírgenes  (Britan</option>                      
                                                                    <option value="Islas Vírgenes (U.S.)">Islas Vírgenes (U.S.)</option>                      
                                                                    <option value="Israel">Israel</option>                      
                                                                    <option value="Italia">Italia</option>                      
                                                                    <option value="Jamaica">Jamaica</option>                      
                                                                    <option value="Japón">Japón</option>                      
                                                                    <option value="Jordania">Jordania</option>                      
                                                                    <option value="Kazajstán">Kazajstán</option>                      
                                                                    <option value="Kenya">Kenya</option>                      
                                                                    <option value="Kirguistán">Kirguistán</option>                      
                                                                    <option value="Kiribati">Kiribati</option>                      
                                                                    <option value="Kuwait">Kuwait</option>                      
                                                                    <option value="Líbano">Líbano</option>                      
                                                                    <option value="Lesotho">Lesotho</option>                      
                                                                    <option value="Letonia">Letonia</option>                      
                                                                    <option value="Liberia">Liberia</option>                      
                                                                    <option value="Libia">Libia</option>                      
                                                                    <option value="Liechtenstein">Liechtenstein</option>                      
                                                                    <option value="Lituania">Lituania</option>                      
                                                                    <option value="Luxemburgo">Luxemburgo</option>                      
                                                                    <option value="Mónaco">Mónaco</option>                      
                                                                    <option value="Macao">Macao</option>                      
                                                                    <option value="Macedonia">Macedonia</option>                      
                                                                    <option value="Madagascar">Madagascar</option>                      
                                                                    <option value="Malawi">Malawi</option>                      
                                                                    <option value="Malaysia">Malaysia</option>                      
                                                                    <option value="Maldivas">Maldivas</option>                      
                                                                    <option value="Mali">Mali</option>                      
                                                                    <option value="Malta">Malta</option>                      
                                                                    <option value="Marruecos">Marruecos</option>                      
                                                                    <option value="Martinica">Martinica</option>                      
                                                                    <option value="Mauricio">Mauricio</option>                      
                                                                    <option value="Mauritania">Mauritania</option>                      
                                                                    <option value="Mayotte">Mayotte</option>                      
                                                                    <option value="Mexico">Mexico</option>                      
                                                                    <option value="Micronesia">Micronesia</option>                      
                                                                    <option value="Moldavia">Moldavia</option>                      
                                                                    <option value="Mongolia">Mongolia</option>                      
                                                                    <option value="Montserrat">Montserrat</option>                      
                                                                    <option value="Mozambique">Mozambique</option>                      
                                                                    <option value="Myanmar">Myanmar</option>                      
                                                                    <option value="Níger">Níger</option>                      
                                                                    <option value="Namibia">Namibia</option>                      
                                                                    <option value="Nauru">Nauru</option>                      
                                                                    <option value="Nepal">Nepal</option>                      
                                                                    <option value="Nicaragua">Nicaragua</option>                      
                                                                    <option value="Nigeria">Nigeria</option>                      
                                                                    <option value="Niue">Niue</option>                      
                                                                    <option value="Noruega">Noruega</option>                      
                                                                    <option value="Nueva Caledonia">Nueva Caledonia</option>                      
                                                                    <option value="Nueva Zelanda">Nueva Zelanda</option>                      
                                                                    <option value="Oman">Oman</option>                      
                                                                    <option value="Paises Bajos">Paises Bajos</option>                      
                                                                    <option value="Pakistan">Pakistan</option>                      
                                                                    <option value="Palaos">Palaos</option>                      
                                                                    <option value="Panama">Panama</option>                      
                                                                    <option value="Papúa Nueva Guinea">Papúa Nueva Guinea</option>                      
                                                                    <option value="Paraguay">Paraguay</option>                      
                                                                    <option value="Peru">Peru</option>                      
                                                                    <option value="Polinesia  Francesa">Polinesia  Francesa</option>                      
                                                                    <option value="Polonia">Polonia</option>                      
                                                                    <option value="Portugal">Portugal</option>                      
                                                                    <option value="Puerto Rico">Puerto Rico</option>                      
                                                                    <option value="Qatar">Qatar</option>                      
                                                                    <option value="Reino Unido">Reino Unido</option>                      
                                                                    <option value="República  Checa">República  Checa</option>                      
                                                                    <option value="República Centroafrican">República Centroafrican</option>                      
                                                                    <option value="República Popular Lao">República Popular Lao</option>                      
                                                                    <option value="Reunión">Reunión</option>                      
                                                                    <option value="Ruanda">Ruanda</option>                      
                                                                    <option value="Rumanía">Rumanía</option>                      
                                                                    <option value="Sáhara Occidental">Sáhara Occidental</option>                      
                                                                    <option value="Samoa">Samoa</option>                      
                                                                    <option value="San Cristóbal y Nieves">San Cristóbal y Nieves</option>                      
                                                                    <option value="San Marino">San Marino</option>                      
                                                                    <option value="San Pedro y  Miquelón">San Pedro y  Miquelón</option>                      
                                                                    <option value="Santa Elena">Santa Elena</option>                      
                                                                    <option value="Santa Lucía">Santa Lucía</option>                      
                                                                    <option value="Santo Tomé y Principe">Santo Tomé y Principe</option>                      
                                                                    <option value="Senegal">Senegal</option>                      
                                                                    <option value="Seychelles">Seychelles</option>                      
                                                                    <option value="Sierra Leona">Sierra Leona</option>                      
                                                                    <option value="Singapur">Singapur</option>                      
                                                                    <option value="Siria">Siria</option>                      
                                                                    <option value="Somalia">Somalia</option>                      
                                                                    <option value="Sri Lanka">Sri Lanka</option>                      
                                                                    <option value="Sudáfrica">Sudáfrica</option>                      
                                                                    <option value="Sudán">Sudán</option>                      
                                                                    <option value="Suecia">Suecia</option>                      
                                                                    <option value="Suiza">Suiza</option>                      
                                                                    <option value="Surinam">Surinam</option>                      
                                                                    <option value="Svalbard And Jan Mayen Islands">Svalbard And Jan Mayen Islands</option>                      
                                                                    <option value="Swaziland">Swaziland</option>                      
                                                                    <option value="Túnisia">Túnisia</option>                      
                                                                    <option value="Tailandia">Tailandia</option>                      
                                                                    <option value="Taiwan">Taiwan</option>                      
                                                                    <option value="Tanzania">Tanzania</option>                      
                                                                    <option value="Tayikistán">Tayikistán</option>                      
                                                                    <option value="Territorios del Sur Frances">Territorios del Sur Frances</option>                      
                                                                    <option value="Togo">Togo</option>                      
                                                                    <option value="Tokelau">Tokelau</option>                      
                                                                    <option value="Tonga">Tonga</option>                      
                                                                    <option value="Trinidad y Tobago">Trinidad y Tobago</option>                      
                                                                    <option value="Turkmenistán">Turkmenistán</option>                      
                                                                    <option value="Turquía">Turquía</option>                      
                                                                    <option value="Tuvalu">Tuvalu</option>                      
                                                                    <option value="Ucrania">Ucrania</option>                      
                                                                    <option value="Uganda">Uganda</option>                      
                                                                    <option value="Uruguay">Uruguay</option>                      
                                                                    <option value="Uzbekistán">Uzbekistán</option>                      
                                                                    <option value="Vanuatu">Vanuatu</option>                      
                                                                    <option value="Venezuela">Venezuela</option>                      
                                                                    <option value="Viet Nam">Viet Nam</option>                      
                                                                    <option value="Wallis And Futuna Islands">Wallis And Futuna Islands</option>                      
                                                                    <option value="Yemen">Yemen</option>                      
                                                                    <option value="Yibuti">Yibuti</option>                      
                                                                    <option value="Zaire">Zaire</option>                      
                                                                    <option value="Zambia">Zambia</option>                      
                                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                                </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Fecha de nacimiento:</label>
                                                                    <input type="date" class="form-control" name="fechanac" id="fechanac" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Correo:</label>
                                                                    <input type="email" class="form-control" placeholder="correo@mail.com" name="email" id="email" maxlength="50" required>
                                                                    <i class="fa fa-envelope-o form-control-feedback"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Celular:</label>
                                                                    <input type="text" class="form-control" placeholder="952888888" name="celular" id="celular" required maxlength="15">
                                                                    <i class="fa fa-mobile-phone form-control-feedback"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="form-group has-success has-feedback">
                                                                    <label>Egresado UPeU:</label> <br>
                                                                    <label>Si</label>
                                                                    <input type="radio" name="egresado" value="si" id="egresado" required="" />
                                                                    <br>
                                                                    <label>No</label>
                                                                    <input type="radio" name="egresado" value="no" id="egresado" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-2">
                                                                <div class="">
                                                                    <label>Adjuntar Boucher:</label>
                                                                    <input type="file" class="form-control" name="boucher" id="boucher" required="">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="boton" value="registrar">
                                                            <!--<div class="col-md-8 col-md-offset-2"></div>-->
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit"  name="addPsico" class="button-enviar btn btn-lg btn-dark btn-animated margin-clear" id="">Registrar<i class="fa fa-send"></i></button>
                                                        <!--<button type="button" class="btn btn-default" data-dissmiss="modal">Enviar</button>-->
                                                    </div>
                                                </form>
                                            </div>
                                            <script>
                                                $(document).on("ready",function(){
                                                    
                                                });
                                                function registrar(){
                                                    var formData = new FormData($("#formInscripcion")[0]);
                                                    $.ajax({
                                                        url: "../app/eventoController.php",
                                                        type: 'POST',
                                                        data: formData,
                                                        cache: false,
                                                        processData: false,
                                                        contentType: false,
                                                    }).done(function(resp){
                                                        alert(resp);
                                                    })
                                                }
                                                
                                            </script>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- main end -->

                    </div>
                </div>
            </section>
            <!-- main-container end -->

            <!-- section start -->
            <!-- ================ -->
            <section  id="collapseContent" class="collapse pv-20 dark-bg clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center"><strong>Módulos</strong></h2>
                        </div>
                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>1</strong></i></span>
                                <h5>Conceptos Generales sobre Problemas de aprendizaje</h5>
                                <div class="separator clearfix"></div>

                                <ul>
                                    <li style="text-align:left">Aprendizaje</li>
                                    <li style="text-align:left">Dificultades de Aprendizaje</li>
                                    <li style="text-align:left">Criterios utilizados en la definición de las dificultades de aprendizaje</li>
                                    <li style="text-align:left">Posibles causas que provocan los trastornos de aprendizaje</li>
                                    <li style="text-align:left">Clasificación de las dificultades de aprendizaje.</li>
                                    <li style="text-align:left">Etiología de las dificultades de aprendizaje.</li>
                                </ul>

                                <!--<a class="button btn btn-gray-transparent radius-50 btn-animated">Leer más<i class="fa fa-angle-double-right"></i></a>-->

                            </div>
                        </div>

                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>2</strong></i></span>
                                <h5>La evaluación de las dificultades de aprendizaje</h5>
                                <div class="separator clearfix"></div>
                                <ul>
                                    <li style="text-align:left">Evaluación desde la perspectiva neuropsicológica</li>
                                    <li style="text-align:left">Evaluación desde perspectiva conductual</li>
                                    <li style="text-align:left">Evaluación desde el enfoque cognitivo</li>
                                    <li style="text-align:left">Pruebas en Inicial</li>
                                </ul>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>


                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>3</strong></i></span>
                                <h5>Comportamiento, cognición, cerebro y dificultades específicas de aprendizaje.</h5>
                                <div class="separator clearfix"></div>
                                <ul>
                                    <li style="text-align:left">Reconocimiento de palabras escritas</li>
                                    <li style="text-align:left">Fluidez lectora</li>
                                    <li style="text-align:left">Comprensión lectora</li>
                                    <li style="text-align:left">Escritura de palabras</li>
                                    <li style="text-align:left">Matemáticas</li>
                                </ul>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered    text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>4</strong></i></span>
                                <h5>Medidas estructurales de respuesta a las DA</h5>
                                <div class="separator clearfix"></div>
                                <ul>
                                    <li style="text-align:left">Algunas medidas de centro relativas al diseño del currículo</li>
                                    <li style="text-align:left">Medidas relativas a la organización del centro</li>
                                </ul>

                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>

                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered    text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>5</strong></i></span>
                                <h5>Adaptaciones inespecíficas y refuerzo educativo en el aula</h5>
                                <div class="separator clearfix"></div>

                                <ul>
                                    <li style="text-align:left">Adaptaciones relacionadas con los contenidos</li>
                                    <li style="text-align:left">Modificación del nivel de abstracción</li>
                                    <li style="text-align:left">Modificación del nivel de complejidad</li>
                                    <li style="text-align:left">Refuerzo educativo</li>
                                    <li style="text-align:left">Condiciones para el refuerzo educativo.</li>

                                </ul>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                                <p style="text-align:left">&nbsp; </p>
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="pv-30 ph-20 feature-box bordered    text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg"><i class=""><strong>6</strong></i></span>
                                <h5>Evaluación Psicopedagógica en el nivel primaria</h5>
                                <div class="separator clearfix"></div>
                                <ul>
                                    <li style="text-align:left">Reciben información de las fichas técnicas y uso de las pruebas</li>
                                    <li style="text-align:left">Evalúa 0</li>
                                    <li style="text-align:left">Evalúa 1</li>
                                    <li style="text-align:left">Evalúa 2</li>
                                    <li style="text-align:left">Evalúa 3</li>
                                    <li style="text-align:left">Evalúa 4</li>

                                </ul>

                            </div>
                        </div>
                    </div>

            </section>

            <section id="collapseContent" class="collapse pv-20 light-gray-bg clearfix">
                <div class="container">
                    <h3><strong>Módulos</strong></h3>
                    <div class="separator-2 mb-20"></div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 1</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Generando conversaciones para el cambio</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Abril, 2017 </p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Esta semana se introducirán las nociones fundamentales para comprender cómo es que construimos realidades terapéuticas para decidir dónde aplicar estos conceptos...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer mas<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-2.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 2</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Iniciando la conversación y marcando una dirección</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Abril, 2017 </p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Descubriremos la forma de comenzar a construir soluciones a través de conversaciones constructivas que incluyen un método de negociación de objetivos y una plataforma ...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer más<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-3.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 3</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Escalas y excepciones</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Abril, 2017 <i class="pl-10 icon-tag-1"></i> </p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Revelaremos la importancia de descubrir aquello que funciona y es útil en la vida de las personas. Lo haremos aprendiendo a descubrir las excepciones, un abordaje vital ...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer más<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-3.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 4</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Hacer que las cosas sucedan</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Abril, 2017 <i class="pl-10 icon-tag-1"></i> </p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Analizaremos la pragmática de las soluciones a través de las conversaciones que construyen y afirman los deseos de las personas a través de la creación de pequeños pero decisivos pasos ...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer más<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-3.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 5</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Todo en conjunto</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Abril, 2017  <i class="pl-10 icon-tag-1"></i></p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Ampliaremos nuestro entendimiento sobre la aplicación del modelo, sus principios y los abordajes explorados los módulos previos...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer más<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="overlay-container">
                                    <img src="images/portfolio-3.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em>Módulo 6</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-9">
                                <div class="body">
                                    <h3 class="title">Mantener la conversación centrada en soluciones</h3>
                                    <p class="small mb-10"><i class="icon-calendar"></i> Feb, 2015 <i class="pl-10 icon-tag-1"></i> Web Design</p>
                                    <div class="separator-2"></div>
                                    <p class="mb-10">Finalizaremos el curso explorando nuestro aprendizaje y qué tan lejos hemos llegado. Exploraremos cómo podemos seguir configurando acciones ...</p>
                                    <a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Leer más<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <!-- section end -->

            <!-- footer top start -->
            <!-- ================ -->
            <div class="dark-bg  default-hovered footer-top animated-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="call-to-action text-center">
                                <div class="row">
                                    <div class="col-sm-12" >
                                        <h2>APROVECHA ESTA OPORTUNIDAD</h2>
                                        <h2>PERFECCIONATE Y ACTUALIZATE</h2>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer top end -->


            <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
            <!-- ================ -->
            <footer id="footer" class="clearfix ">

                <!-- .footer start -->
                <!-- ================ -->
                <div class="footer">
                    <div class="container">
                        <div class="footer-inner">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="footer-content">
                                        <h3 class="title"><strong>Dirigido a:</strong></h3>
                                        <div class="separator-2"></div>
                                        <p align="justify">Psicólogos y docentes. </p>
                                        <h3 class="title"><strong>Lugar:</strong></h3>
                                        <p>Colegio Adventista Miraflores – Av. Comandante Espinar 750 | Miraflores </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <h3 class="title"><strong>Fechas y Horarios</strong></h3>
                                    <div class="separator-2"></div>
                                    <table class="table table-hover">

                                        <thead>
                                            <tr>
                                                <th class="product" colspan="2"><strong>Domingos | 9:00am a 6:00pm</strong></th>
                                            </tr>
                                            <tr>
                                                <th class="amount"><i class="fa fa-calendar pr-10"></i>25 Junio</th>
                                                <th class="amount"><i class="fa fa-calendar pr-10"></i>2 Julio</th>
                                            </tr>
                                            <tr>
                                                <th class="amount"><i class="fa fa-calendar pr-10"></i>9 Julio</th>
                                                <th class="amount"><i class="fa fa-calendar pr-10"></i>16 Julio</th>
                                            </tr>

                                        </thead>
                                    </table>
                                    <br>
                                    <h3 class="title"><strong>Inversión</strong></h3>
                                    <div class="separator-2"></div>
                                    <table class="table table-hover">


                                        <thead>
                                            <tr>
                                                <th class="product"><strong>Derecho de enseñanza | S/750.00</strong></th>     
                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                                <div class="col-md-3">
                                    <div class="footer-content">
                                        <h2 class="title"><strong>Modalidad de pago</strong></h2>
                                        <div class="separator-2"></div>
                                        <center>
                                            <p class="media-heading"><i class="fa fa-money  pr-10"></i><strong>Pago en efectivo - Caja de Escuela Posgrado</strong></p>
                                        </center>
                                        <div class="media margin-clear">
                                            <div class="media-left">
                                                <div class="overlay-container">
                                                    <img class="media-object" src="../images/upgpsico/logo upeu.png" alt="blog-thumb">
                                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="media-body">

                                                <p class="media-heading">Tarjeta de Crédito:</p>
                                                <p class="media-heading">Visa y Master Card.</p>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="separator-2"></div>
                                        <div class="media margin-clear">
                                            <center><strong>Depósitos Nacionales</strong></center>
                                            <div class="media-left">
                                                <div class="overlay-container">
                                                    <img class="media-object" src="../images/upgpsico/bcp-logo.jpg" alt="blog-thumb">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <p class="media-heading">N.º Cta. Cte. Soles</p>
                                                <p class="media-heading">193-0811892-0-28</p>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="media margin-clear">
                                            <div class="media-left">
                                                <div class="overlay-container">
                                                    <img class="media-object" src="../images/upgpsico/bbva-logo1.png" alt="blog-thumb">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <p class="media-heading">N.º Cta. Cte. Soles </p>
                                                <p class="media-heading">0011-0661-01-00039316</p>

                                            </div>
                                            <br>
                                        </div>
                                        <div class="media margin-clear">
                                            <center><strong>Depósitos Extranjeros</strong></center>
                                            <div class="media-left">
                                                <div class="overlay-container">
                                                    <img class="media-object" src="../images/upgpsico/wester.gif" alt="blog-thumb">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <p class="media-heading">Extranjeros</p>
                                                <p class="media-heading">N.º Cta. Cte. Dolares </p>
                                                <p class="media-heading">201-03-00-0862400-8</p>

                                            </div>
                                            <br>
                                        </div>
                                        <div class="media margin-clear">
                                            <center>
                                                <div class="media-body">
                                                    <p class="media-heading"><strong>Enviar voucher scaneado al correo</strong></p>
                                                    <p class="media-heading"><strong>eventosposgrado@upeu.edu.pe</strong></p>
                                                </div>
                                            </center>
                                            <br>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="footer-content">
                                        <h2 class="title"><strong>Certificación</strong></h2>
                                        <div class="separator-2"></div>
                                        <center>
                                            <img src="../images/upgpsico/medalla.png" width="10%" height="10%" />
                                            <p class="media-heading"><strong>80 horas académicas a nombre de la Escuela de Posgrado</strong></p>
                                        </center>

                                        <br>
                                        <h2 class="title"><strong>Síguenos</strong></h2>
                                        <div class="separator-2"></div>

                                        <ul class="social-links circle animated-effect-1">

                                            <li class="facebook"><a target="" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a target="" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .footer end -->

                <!-- .subfooter start -->
                <!-- ================ -->
                <div class="subfooter default-bg">
                    <div class="container">
                        <div class="subfooter-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center">UPeU | Escuela de Posgrado 2017</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .subfooter end -->

            </footer>
            <!-- footer end -->

        </div>
        <!-- page-wrapper end -->

        <!-- JavaScript files placed at the end of the document so the pages load faster -->
        <!-- ================================================== -->
        <!-- Jquery and Bootstap core js files -->
        <script type="text/javascript" src="../plugins/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- Modernizr javascript -->
        <script type="text/javascript" src="../plugins/modernizr.js"></script>
        <!-- Magnific Popup javascript -->
        <script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        <!-- Appear javascript -->
        <script type="text/javascript" src="../plugins/waypoints/jquery.waypoints.min.js"></script>
        <!-- Count To javascript -->
        <script type="text/javascript" src="../plugins/jquery.countTo.js"></script>
        <!-- Parallax javascript -->
        <script src="../../plugins/jquery.parallax-1.1.3.js"></script>
        <!-- Contact form -->
        <script src="../../plugins/jquery.validate.js"></script>
        <!-- Owl carousel javascript -->
        <script type="text/javascript" src="../plugins/owlcarousel2/owl.carousel.min.js"></script>
        <!-- SmoothScroll javascript -->
        <script type="text/javascript" src="../plugins/jquery.browser.js"></script>
        <script type="text/javascript" src="../plugins/SmoothScroll.js"></script>
        <!-- Initialization of Plugins -->
        <script type="text/javascript" src="../js/template.js"></script>
        <!-- Custom Scripts -->
        <script type="text/javascript" src="../js/custom.js"></script>
        <!-- Menu -->
        <script src="../js/include.js"></script>
        <script>!window.jQuery && document.write(unescape('%3Cscript src="jquery-1.7.1.min.js"%3E%3C/script%3E'))</script>
        <script type="text/javascript" src="../js/popup.js"></script>
    </body>

</html>

