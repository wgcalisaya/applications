<?php

if(function_exists('date_default_timezone_get'))
{
    $timezone = date_default_timezone_set('America/Mexico_City');

    if(empty($timezone))
    {
        date_default_timezone_set('America/Mexico_City');
    }
}
else
{
    $timezone = ini_get('date.timezone');

    if(empty($timezone))
    {
        ini_set('date.timezone', 'America/Mexico_City');
    }
}
