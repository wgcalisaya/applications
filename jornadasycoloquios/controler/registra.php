<?php
header('refresh:10; url=http://posgrado.upeu.edu.pe/jornadasycoloquios');
include("../config/conecta.php"); 
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Eventos Cient&iacute;ficos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/templatemo_misc.css">
    <link rel="stylesheet" href="../css/templatemo_style.css">
    <link rel="stylesheet" href="../css/formulariowilly.css">
    


    <script src="../js/vendor/modernizr-2.6.2.min.js"></script>

</head>
<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
	<!--  Free HTML5 Template by http://www.templatemo.com -->
    <div id="front">
        <div class="site-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div id="templatemo_logo">
                            <h1><a rel="nofollow" href="http://posgrado.upeu.edu.pe" title="Unidad de Posgrado de Ciencias de la Salud">UPG Salud</a></h1>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-8 col-sm-6 col-xs-6">
                        <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
                        <div class="main-menu">
                            <ul>
                                <li><a href="#front">Inicio</a></li>
                                <li><a href="#services">Eventos</a></li>
                                <li><a href="#products">Inscripci&oacute;n</a></li>
                                <li><a href="#contact">Contactos</a></li>
                            </ul>
                        </div> <!-- /.main-menu -->
                    </div> <!-- /.col-md-8 -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="responsive">
                            <div class="main-menu">
                                <ul>
                                    <li><a href="#front">Inicio</a></li>
                                    <li><a href="#services">Eventos</a></li>
                                    <li><a href="#products">Inscripci&oacute;n</a></li>
                                    <li><a href="#contact">Contactos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.site-header -->
    </div> <!-- /#front -->


    <div id="products" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Inscripci&oacute;n al Evento</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
            	<div class="col-md-12 col-sm-6" align="center">
                    
                    <form class="contact_form" enctype="multipart/form-data" action="controler/registra.php" method="post" name="contact_form">
                    	<ul>
                            <li>
                                 <font><h2>Formulario de Inscripci&oacute;n</h2>
                                 <span class="required_notification"><img src="../images/red_asteriskoo.png" title=""> Significa campo obligatorio</span></font>
                            </li>
					</form>
                    
                    	<fieldset class="col-md-12">
<?php
$uploadedfileload="true";
$uploadedfile_size=$_FILES['uploadedfile']['size'];
echo "Inscripci&oacute;n satisfactoria al evento, el archivo ".$_FILES['uploadedfile']['name'];
if ($_FILES['uploadedfile']['size']>200000000)
{$msg=$msg."El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<BR>";
$uploadedfileload="false";}

if (!($_FILES['uploadedfile']['type']=="image/jpg" OR $_FILES['uploadedfile']['type']=="image/jpeg" OR $_FILES['uploadedfile']['type']=="image/png" OR $_FILES['uploadedfile']['type']=="image/gif"))
{$msg=$msg." Tu archivo tiene que ser JPG, JPEG, PNG y GIF. Otros archivos no son permitidos<BR>";
$uploadedfileload="false";}

$rand=rand(1000,999999);
$file_name=$_FILES['uploadedfile']['name'];
$add="../voucherjornada/".$rand."_".$file_name;
//$add="../voucherjornada/".$file_name;

if($uploadedfileload=="true"){

if(move_uploaded_file ($_FILES['uploadedfile']['tmp_name'], $add)){

$nombrep=$_POST['nombrep'];
$nombres=$_POST['nombres'];
$apellidop=$_POST['apellidop'];
$apellidom=$_POST['apellidom'];
$iniciales=$_POST['iniciales'];
$filiacion=$_POST['filiacion'];
$participacion=$_POST['participacion'];
$procedencia=$_POST['procedencia'];
$email=$_POST['email'];
$dni=$_POST['dni'];
$programa=$_POST['programa'];
$fecha=date("Y-m-d H:i:s");
$sql="INSERT INTO eventos (id, nombrep, nombres, apellidop, apellidom, iniciales, filiacion, participacion, procedencia, email, dni, programa, voucher, fecha) 
	  VALUES ('','$nombrep', '$nombres', '$apellidop', '$apellidom','$iniciales', '$filiacion', '$participacion', '$procedencia','$email', '$dni', '$programa', '$add', '$fecha')";
mysql_query($sql);
/**/
echo " fue subido correctamente";
}else{echo "Error al subir el archivo"; echo ' ]'.$add.' ]';}

}else{echo $msg;}
?>
                    	</fieldset>
                        <fieldset class="col-md-12">
<?php
echo "<img src='$add' width='40%' height='40%' >";
?>
                        </fieldset>
                        
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->


    <div class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <span>Copyright &copy; 2014 <a href="#">Willy Jhon Medina Bacalla</a> <!-- Credit: www.templatemo.com --></span>
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <ul class="social">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                        <li><a href="#" class="fa fa-rss"></a></li>
                    </ul>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.site-footer -->

    
    <script src="../js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="../js/jquery.easing-1.3.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/main.js"></script>
    <!--  Free HTML5 Template by http://www.templatemo.com -->
    <!-- templatemo 401 sprint -->
<!-- 
Sprint Template 
http://www.templatemo.com/preview/templatemo_401_sprint 
-->
</body>
</html>