<?php
require_once('conexion.php');

class ListaTotal extends Conexion{

	public function listot()
	{
		parent::conectar();

		$consultalist = 'select COUNT(*) as total from persona';
		$verificar_lista = parent::query($consultalist);

		$arr = array();

		if(mysqli_num_rows($verificar_lista) > 0){

			while ($row =  mysqli_fetch_assoc($verificar_lista)) {
				$arr[] = $row;
			}
		}
		echo json_encode($arr);
	}

}

?>
