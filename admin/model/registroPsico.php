<?php

require_once('conexion.php');

class Listapsi extends Conexion {

	public function listar()
	{
		parent::conectar();
                
                $psico = $_SESSION['cargo'];
                
		$consultalist = 'select * from persona where tipevent = '.$psico.'';
		$verificar_lista = parent::query($consultalist);

        $arr = array();

        if (mysqli_num_rows($verificar_lista) > 0) {

            while ($filas = mysqli_fetch_assoc($verificar_lista)) {
                $arr[] = $filas;
            }
        }
        echo json_encode($arr);
        parent::cerrar();
    }

    public function buscarpsi($id) {
        parent::conectar();
        $consultaper = 'select persona_id, person_nombre, person_email from persona where persona_id = ' . $id . '';

        $result = parent::query($consultaper);

        while ($filas = mysqli_fetch_array($result)) {
            $arr[] = $filas;
        }
        echo json_encode($arr);
        //echo $result;
        parent::cerrar();
    }

    public function enviarcorreoconfirm($id, $destino, $nombre) {
        parent::conectar();
        $destino = $destino;
        $nombredestino = $nombre;
        $mensaje = '
            <html lang="es">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body style="background-color: #b3b3b3 ">
        <!--Copia desde aquí-->
        <table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
            <tr>
                <td style="background-color: #ecf0f1; text-align: left; padding: 0">
                    <a href="#">
                        <img width="40%" style="display:block; margin: 1.5% 3%" src="https://s3.amazonaws.com/files.patmos.upeu.edu.pe/img/upload/epg/paginas/8490_1488820275.png">
                    </a>
                </td>
            </tr>
            <tr>
                <td style="padding: 0">
                    <img style="padding: 0; display: block;" src="https://patmos.upeu.edu.pe/img/bg/epg/epgupeu.jpg" width="100%">
                </td>
            </tr>
            <tr>
                <td style="background-color: #ecf0f1">
                    <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                        <h2 style="color: #f9a825 ; position: absolute ; text-shadow: -2px -1px 10px #aaa ;top: 200px; left: 500px;">Escuela de Posgrado | UPeU</h2>
                        <p style="margin: 2px; font-size: 15px">Estimado(a): '.$nombredestino.'
                        </p>
                        <p style="margin: 2px; font-size: 15px">
                            Bienvenido, hemos recibido tus datos de inscripcion, y te confirmamos que ha sido recibido con exito.</p>
                        <ul style="font-size: 15px;  margin: 10px 0">
                            <li>Boucher de posito con monto de: S/. 2,500</li>
                            <li>Datos correctos.</li>
                        </ul>
                        <p style="margin: 2px; font-size: 15px">
                            Comentarte que las clases empezaran el dia 12/12/12 y tu entrevista sera el dia 11/12/12.</p>
                        <div style="width: 100%;margin:20px 0; display: inline-block;text-align: center">
                        </div>
                        <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UPeU</p>
                    </div>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
    </body>
</html>
            ';

        $headers = "From: sistemas.epg@upeu.edu.pe";
        $headers .= "X-Mailer: Escuela de Posgrado | UPeU \r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        mail($destino, "Correo de confirmacion Escuela de Posgrado UPeU", $mensaje, $headers);

        $cambiarestado = 'UPDATE persona SET estadomail = "si" WHERE persona_id = ' . $id . '';
        $cambiarstate = parent::query($cambiarestado);
        
        
        
        //if (mysqli_num_rows($cambiarstate) > 0) {
            $respuesta = 'El correo fue enviado a ' . trim($nombredestino) . '.';
            echo $respuesta;
        //}
        parent::cerrar();
    }
}

?>
