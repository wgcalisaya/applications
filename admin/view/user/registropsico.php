<?php
  // Se prendio esta mrd :v
  session_start();

  // Validamos que exista una session y ademas que el cargo que exista sea igual a 1 (Administrador)
  if(!isset($_SESSION['cargo']) || $_SESSION['cargo'] != 2){
    /*
      Para redireccionar en php se utiliza header,
      pero al ser datos enviados por cabereza debe ejecutarse
      antes de mostrar cualquier informacion en el DOM es por eso que inserto este
      codigo antes de la estructura del html, espero haber sido claro
    */
    header('location: ../../index.php');
  }

?>

<!DOCTYPE html>
<html lang="es" ng-app="crudApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Psicologia</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/metisMenu.min.css" rel="stylesheet">
    <link href="../../css/timeline.css" rel="stylesheet">
    <link href="../../css/startmin.css" rel="stylesheet">
    <link href="../../css/morris.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/sweetalert.css">
    <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../../js/jquery.js"></script>
    <script src="../../js/sweetalert.min.js"></script>
    <script src="../../js/angular.min.js"></script>
</head>

<body ng-controller="DbControllerPsic">

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><i class="fa fa-home fa-fw"></i>Psicologia</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <?php echo ucfirst($_SESSION['nombre']); ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li class="divider"></li>
                    <li><a href="../../controller/cerrarSesion.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </li>
                    <li>
                        <a href="registrados.php"><i class="fa fa-table fa-fw"></i>Inscritos psicologia</a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Inscritos Psicologia</h1>
                </div>
            </div>

            <!-- ... Your content goes here ... -->

            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <div class="alert alert-default input-group search-box">
                        <span class="input-group-btn">
                            <input type="text" class="form-control" placeholder="Buscar por Nombres ó Apellidos" ng-model="search_query">
                        </span>
                    </div>
                </div>
            </nav>

            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Nombres</th>
                        <th>Tipo Doc</th>
                        <th>Nro Doc</th>
                        <th>Nacionalidad</th>
                        <th>Fecha Nac</th>
                        <th>Correo</th>
                        <th>Celular</th>
                        <th>Egresado UPeU</th>
                        <th>Boucher</th>
                        <th>Correo Respondido</th>
                        <th>Responder</th>
                        
                    </tr>
                    {{enviado.data}}
<!--                    <li ng-repeat="ok in encontrado.data">{{ok.person_nombre}}</li>-->
                    
                    <tr ng-repeat="detail in details | filter:search_query">
                        <td>{{detail.persona_id}}</td>
                        <td>{{detail.person_nombre}}</td>
                        <td>{{detail.tipodoc}}</td>
                        <td>{{detail.nrodoc}}</td>
                        <td>{{detail.nacionalidad}}</td>
                        <td>{{detail.fechanac}}</td>
                        <td>{{detail.person_email}}</td>
                        <td>{{detail.celular}}</td>
                        <td>{{detail.egresado}}</td>
                        <td><a target="../" href="../../../app/picture/{{detail.person_file}}" title="">Ver</a></td>
                        <td ng-if="detail.estadomail == 'si'"><span class="glyphicon glyphicon-ok"></span></td>
                        <td ng-if="detail.estadomail == 'no'"><span class="glyphicon glyphicon-ban-circle"></td>
                        <td><button class="btn btn-success" ng-click="retorname(detail)" title="Delete"><span class="glyphicon glyphicon-send"></span></button></td>

                    </tr>
                </table>
            </div>

        </div>
    </div>

</div>

<!-- jQuery -->
<script src="../js/personas.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/metisMenu.min.js"></script>
<script src="../../js/startmin.js"></script>

</body>
</html>