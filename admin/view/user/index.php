<?php
  session_start();
  // Validamos que exista una session y ademas que el cargo que exista sea igual a 1 (Administrador)
  if(!isset($_SESSION['cargo']) || $_SESSION['cargo'] != 0){
    header('location: ../../index.php');
  }

?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>User</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <script src="../../js/jquery.js"></script>
    <script src="../../js/angular.min.js"></script>
  </head>
  <body>
    <!-- ucfirst convierte la primera letra en mayusculas de una cadena -->

    <h1 class="text-center">BIENVENIDO <?php echo ucfirst($_SESSION['nombre']); ?>, Comunicate con el area de TI
        <br>
        989059411
        <br>
        <a href="../../controller/cerrarSesion.php">
      <button type="button" name="button">Cerrar sesion</button>
    </a>
    </h1>

    
            
  </body>
</html>
