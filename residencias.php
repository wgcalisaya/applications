<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="en">
    <!--<![endif]-->
    <script src="js/include.js"></script>	

    <head>
        <meta charset="utf-8">
        <title>Escuela de Posgrado | UPeU</title>
        <meta name="description" content="Profesionales alatamente confiables">
        <meta name="author" content="htmlcoder.me">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Web Fonts -->

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">


        <!-- Font Awesome CSS -->
        <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Fontello CSS -->
        <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

        <!-- Plugins -->
        <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
        <link href="css/animations.css" rel="stylesheet">
        <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="plugins/hover/hover-min.css" rel="stylesheet">		
        <link href="plugins/morphext/morphext.css" rel="stylesheet">
        <link rel="stylesheet" href="admin/css/sweetalert.css">

        <!-- The Project's core CSS file -->
        <!-- Use css/rtl_style.css for RTL version -->
        <link href="css/style.css" rel="stylesheet" >
        <!-- The Project's Typography CSS file, includes used fonts -->
        <!-- Used font for body: Roboto -->
        <!-- Used font for headings: Raleway -->
        <!-- Use css/rtl_typography-default.css for RTL version -->
        <link href="css/typography-default.css" rel="stylesheet" >
        <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
        <link href="css/skins/gold.css" rel="stylesheet">


        <!-- Custom css --> 
        <link href="css/custom.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <!-- "gradient-background-header": applies gradient background to header -->
    <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
    <body class="no-trans front-page   page-loader-2">

        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">

            <!-- header-container start -->
            <div class="header-container">


                <!-- ================ --> 
                <div w3-include-html="header.html"></div>

                <script>
                    w3IncludeHTML();
                </script>
                <!-- header end -->
            </div>
            <!-- header-container end -->

            <!-- banner start -->
            <!-- ================ -->
            <div class="banner banner-big-height dark-translucent-bg padding-bottom-clear" style="background-image:url('images/parque.jpg');background-position: 50% 32%;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 text-center col-md-offset-2 pv-20">
                            <h1 class="title">Reservar residencia | 2017-II </h1>
                            <div class="separator mt-10"></div>
                        </div>
                    </div>
                </div>
                <!-- section start -->
                <!-- ================ -->
                <div class="dark-translucent-bg section">
                    <div class="container">
                        <!-- filters start -->

                        <!-- INICIO -->
                        <!-- INICIO -->
                        <div class="sorting-filters text-center mb-20">
                            <form name="formulario" id="formulario" class="form-inline">

                                <div class="form-group">
                                    <label style="text-align:center">Nombre Completo *</label>
                                    <input type="text" name="person_nombre" id="person_nombre"  placeholder=""  size="27" maxlength="100" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Codigo estudiante *</label>
                                    <input type="text" name="person_code" id="person_code"  placeholder=""  size="10" maxlength="10" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Seleccione Unidad *</label>
                                    <select class="form-control" name="person_unidad" id="person_unidad" placeholder="" required>
                                        <option value="null">--Elegir--</option>
                                        <option value="PCE">Posgrado Ciencias Empresariales</option>
                                        <option value="PCHE">Posgrado Ciencias Humanas y Educación</option>
                                        <option value="PIA">Posgrado de Ingeniería y Arquitectura</option>
                                        <option value="PP">Posgrado Psicología</option>
                                        <option value="PT">Posgrado Teología</option>		  
                                    </select>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Email *</label>
                                    <input type="text" name="person_email" id="person_email" placeholder="correo@mail.com"  size="50" maxlength="100" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label style="text-align:center">Lugar de procedencia *</label>
                                    <input type="text" name="person_lugar" id="person_lugar" placeholder="Ciudad-Pais"  size="30" maxlength="100" class="form-control" required>
                                </div>


                                <br>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Tipo de servicio *</label>
                                    <select class="form-control" name="person_habit" id="person_habit" placeholder="" required>
                                        <option value="null">--Elegir según el cuadro--</option>
                                        <option value="Normal (residencia, alimentación y lavandería)">Normal (residencia, alimentación y lavandería)</option>
                                        <option value="Económico 1 (residencia  y alimentación)">Económico 1 (residencia  y alimentación)</option>
                                        <option value="Económico 2 (residencia y lavandería)">Económico 2 (residencia y lavandería)</option>
                                        <option value="Económico 3 (sólo residencia)">Económico 3 (sólo residencia)</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Fecha de Ingreso *</label>
                                    <input type="text" class="form-control" name="person_fechin" id="person_fechin" required>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Hora de Ingreso *</label>
                                    <select class="form-control" name="person_horain" id="person_horain" placeholder="" required>
                                        <option value="00:00 am">00:00 am</option>
                                        <option value="1:00 am">1:00 am</option>
                                        <option value="2:00 am">2:00 am</option>
                                        <option value="3:00 am">3:00 am</option>
                                        <option value="4:00 am">4:00 am</option>
                                        <option value="5:00 am">5:00 am</option>
                                        <option value="6:00 am">6:00 am</option>
                                        <option value="7:00 am">7:00 am</option>
                                        <option value="8:00 am">8:00 am</option>
                                        <option value="9:00 am">9:00 am</option>
                                        <option value="10:00 am">10:00 am</option>
                                        <option value="11:00 am">11:00 am</option>
                                        <option value="12:00 pm">12:00 pm</option>
                                        <option value="1:00 pm">1:00 pm</option>
                                        <option value="2:00 pm">2:00 pm</option>
                                        <option value="3:00 pm">3:00 pm</option>
                                        <option value="4:00 pm">4:00 pm</option>
                                        <option value="5:00 pm">5:00 pm</option>
                                        <option value="6:00 pm">6:00 pm</option>
                                        <option value="7:00 pm">7:00 pm</option>
                                        <option value="8:00 pm">8:00 pm</option>
                                        <option value="9:00 pm">9:00 pm</option>
                                        <option value="10:00 pm">10:00 pm</option>
                                        <option value="11:00 pm">11:00 pm</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Fecha de salida *</label>
<!--                                    <input type="text" class="form-control" name="person_fechout" id="person_fechout" required>-->
                                    <input type="text" class="form-control" name="person_fechout" id="person_fechout" required>
                                </div>
                                <div class="form-group">
                                    <label style="text-align:center">Hora de Salida *</label>
                                    <select class="form-control" name="person_horaout" id="person_horaout" placeholder="" required>
                                        <option value="00:00 am">00:00 am</option>
                                        <option value="1:00 am">1:00 am</option>
                                        <option value="2:00 am">2:00 am</option>
                                        <option value="3:00 am">3:00 am</option>
                                        <option value="4:00 am">4:00 am</option>
                                        <option value="5:00 am">5:00 am</option>
                                        <option value="6:00 am">6:00 am</option>
                                        <option value="7:00 am">7:00 am</option>
                                        <option value="8:00 am">8:00 am</option>
                                        <option value="9:00 am">9:00 am</option>
                                        <option value="10:00 am">10:00 am</option>
                                        <option value="11:00 am">11:00 am</option>
                                        <option value="12:00 pm">12:00 pm</option>
                                        <option value="1:00 pm">1:00 pm</option>
                                        <option value="2:00 pm">2:00 pm</option>
                                        <option value="3:00 pm">3:00 pm</option>
                                        <option value="4:00 pm">4:00 pm</option>
                                        <option value="5:00 pm">5:00 pm</option>
                                        <option value="6:00 pm">6:00 pm</option>
                                        <option value="7:00 pm">7:00 pm</option>
                                        <option value="8:00 pm">8:00 pm</option>
                                        <option value="9:00 pm">9:00 pm</option>
                                        <option value="10:00 pm">10:00 pm</option>
                                        <option value="11:00 pm">11:00 pm</option>

                                    </select>
                                </div>
                                <br>
                                <br>

                                <div class="form-group">
                                    <label style="text-align:center">Seleccion su boucher</label>
                                    <input type="file" name="person_file" id="person_file" class="form-control">
                                    <input name="boton" type="hidden" value="guardar">
                                </div>
                                <div class="form-group">
                                    <a href="#info1" class="btn btn-warning" >¿Qué tipo de comprabente desea?</a>
                                </div>
                                <div class="row">
                                    <div id="info1" class="oculto">
                                        <p>
                                            <input checked="checked" name="pago1" type="radio" style="width: 25px; height: 25px" value="Factura" id="tipocompf"/>
                                            <span class="auto-style4">Factura</span>

                                            <input  name="pago1" type="radio" style="width: 25px; height: 25px" value="Boleta" id="tipocompb"/>
                                            <span class="auto-style4">Boleta</span>
                                        </p>
                                        <div id="div1">
                                            <div class="form-group">
                                                <input type="text" name="razonsoc"  id="razonsoc" maxlength="49" class="form-control" placeholder="Razon social"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="ruc" id="ruc" maxlength="12" class="form-control" placeholder="RUC"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="direccion" id="direccion" maxlength="95" class="form-control" placeholder="Direccion"/>
                                            </div>
                                        </div>
                                        <div id="div2" style="display:none;">
                                            <div class="form-group">
                                                <input type="text" name="razonsoc2"  id="razonsoc2" maxlength="49" class="form-control" placeholder="Razon social"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label style="text-align:center">Comentario</label>
                                    <textarea rows="3" cols="90" name="comentario" maxlength="140" id="comentario" class="form-control" placeholder="* En caso de tener compañia, escriba sus datos."></textarea>
                                </div>
                                <br>



                                <div class="separator mt-10"></div>


                            </form>
                            (*) Datos obligatorios.
                            <br>
                            <button type="button" id="btnGuardar" onclick="guardar();" name="button_add" style="width:15%"  class="btn btn-lg btn-gray-transparent btn-animated margin-clear" >Enviar<i class="fa fa-check"></i></button>





                            <script type="text/javascript">

                                function guardar() {
                                    var nombre = $('#person_nombre').val();
                                    var codigo = $('#person_code').val();
                                    var person_unidad = $('#person_unidad').val();
                                    var person_email = $('#person_email').val();
                                    var person_lugar = $('#person_lugar').val();
                                    var person_habit = $('#person_habit').val();
                                    var person_fechin = $('#person_fechin').val();
                                    var person_horain = $('#person_horain').val();
                                    var person_fechout = $('#person_fechout').val();
                                    var person_horaout = $('#person_horaout').val();
                                    var file = $('#person_file').val();
                                    
                                    if (nombre == "" || codigo == "" || person_unidad == "" || person_email == "" || person_lugar == "" || person_habit == "" || person_fechin == "" || person_horain == "" || person_fechout == "" || person_horaout == "" || person_fechin == "0000-00-00" || person_fechout == "0000-00-00") {
                                        alert(" >> Uno de los campos esta vacio <<");
                                    } else {

                                        var formData = new FormData($("#formulario")[0]);
                                        $.ajax({
                                            url: 'app/personaController.php?op=guardaryeditar',
                                            type: 'POST',
                                            data: formData,
                                            cache: false,
                                            processData: false,
                                            contentType: false,
                                        }).done(function (resp) {
                                            if (resp == 'ok') {
                                                swal('Gracias', 'Sus datos fueron registrados correctamente.', 'success');
                                            } else {
                                                swal('Error', 'No se pudo registrar, revise sus datos.', 'error');
                                            }
                                            $('#person_nombre').val("");
                                            $('#person_code').val("");
                                            $('#person_unidad').val("");
                                            $('#person_email').val("");
                                            $('#person_lugar').val("");
                                            $('#person_habit').val("");
                                            $('#person_fechin').val("");
                                            $('#person_horain').val("");
                                            $('#person_fechout').val("");
                                            $('#person_horaout').val("");
                                            $('#person_file').val("");
                                            $('#comentario').val("");

                                            $('#razonsoc').val("");
                                            $('#ruc').val("");
                                            $('#direccion').val("");
                                            $('#razonsoc2').val("");
                                        });
                                    }
                                }
                            </script>
                            <script src="admin/js/sweetalert.min.js"></script>
                        </div>
                        <!-- FIN -->
                        <!-- FIN -->
                        <!-- filters end -->
                    </div>
                </div>
                <!-- section end -->

                <div class="col-md-8 text-center col-md-offset-2 pv-20">
                    <table class="table table-striped table-colored">
                        <thead>
                            <tr>

                                <th style="color:#fff">Descripción</th>
                                <th align="center" style="color:#fff"><strong>Costo estudiante</strong></th>
                                <th align="center" style="color:#fff"><strong>Costo familiar</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td align="left" style="color:#000">Normal (residencia, alimentación y lavandería)</td>
                                <td align="center" style="color:#000">S/ 54.00</td>
                                <td align="center" style="color:#000">S/ 64.00</td>
                            </tr>
                            <tr>

                                <td align="left" style="color:#000">Económico 1 (residencia  y alimentación)</td>
                                <td align="center" style="color:#000">S/ 51.00</td>
                                <td align="center" style="color:#000">S/ 60.00</td>
                            </tr>
                            <tr>

                                <td align="left" style="color:#000">Económico 2(residencia y lavandería)</td>
                                <td align="center" style="color:#000">S/ 30.00</td>
                                <td align="center" style="color:#000">S/ 36.00</td>
                            </tr>
                            <tr>

                                <td align="left" style="color:#000">Económico 3 (sólo residencia)</td>
                                <td align="center" style="color:#000">S/ 25.00</td>
                                <td align="center" style="color:#000">S/ 30.00</td>
                            </tr>
                            <tr>

                                <td align="left" style="color:#000">Niños de 2 a 8 años</td>
                                <td >-</td>
                                <td align="center" style="color:#000">S/ 48.00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-8 text-center col-md-offset-2 pv-20">

                    <p>** Si es INDIVIDUAL con habitación normal, se asume el costo total de las 2 personas.</p>
                    <p>** Si es INDIVIDUAL con habitación vip, se asume el costo total de las 4 personas.</p>
                </div>
            </div>
            <!-- banner end -->

            <!-- section start -->
            <!-- ================ --><!-- section end -->

            <!-- section start -->
            <!-- ================ -->

            <!-- section end -->

            <!-- section -->
            <!-- ================ -->
            <section class="section clearfix">
                <div class="container">
                    <div class="tab-content clear-style">
                        <div class="tab-pane active" id="pill-1">

                        </div>
                    </div>


                </div>
            </section>
            <section class="light-gray-bg pv-40">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center">Servicios <strong>UPeU</strong></h2>
                            <div class="separator"></div>
                            <p class="large text-center">La residecia de la UPeU es un ambiente en donde, además de disfrutar de la cercanía de sus aulas, comedor, facultades y áreas administrativas, se encuentra con Dios y desarrolla un carácter puro, que refleje únicamente lo bueno, que es parte de la formación del estudiante que brinda la Universidad Peruana Unión <a href="http://www.upeu.edu.pe/residencia-senoritas/" target="new">Leer más..</a></p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="image-box text-center style-2 mb-20">
<!--                                        <img src="images/restaurant-dish-1.jpg" alt="" class="img-circle img-thumbnail">-->
                                <div class="body padding-horizontal-clear">
                                    <h4 class="logo-font title">Residencia</h4>
                                    <p class="small mb-10">Cambio de ropa de cama semanal, papel higiénico, jaboncillo y champú por semana, wifi.</p>
                                    <p class="lead text-default"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="image-box text-center style-2 mb-20">
<!--                                        <img src="images/restaurant-dish-2.jpg" alt="" class="img-circle img-thumbnail">-->
                                <div class="body padding-horizontal-clear">
                                    <h4 class="logo-font title">Lavandería</h4>
                                    <p class="small mb-10">Lavado de 6 prendas por semana</p>
                                    <p class="lead text-default"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="image-box text-center style-2 mb-20">
<!--                                        <img src="images/restaurant-dish-3.jpg" alt="" class="img-circle img-thumbnail">-->
                                <div class="body padding-horizontal-clear">
                                    <h4 class="logo-font title">Alimentación</h4>
                                    <p class="small mb-10">Alimentación exclusiva del país, buffet.</p>
                                    <p class="lead text-default"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="image-box text-center style-2 mb-20">
<!--                                        <img src="images/restaurant-dish-4.jpg" alt="" class="img-circle img-thumbnail">-->
                                <div class="body padding-horizontal-clear">
                                    <h4 class="logo-font title">Ambiente comodo</h4>
                                    <p class="small mb-10">Incluye todos los servicios mencionados.</p>
                                    <p class="lead text-default"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row grid-space-0">
                        <div class="col-md-6">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/servicios/61036650.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="#">Residencias de Señoritas</a></h3>

                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/servicios/96477360.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3>Areas verdes</h3>
                                            <p class="small"></p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/servicios/96477434.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3>Comedor</h3>
                                            <p class="small"></p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/servicios/comedor.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3>Servicios del comedor</h3>
                                            <p class="small"></p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <!-- section end -->

            <!-- footer top start -->
            <!-- ================ -->
            <div class="dark-bg footer-top animated-text default-hovered">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="call-to-action text-center">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h1 class="title">Bienvenidos a la UPeU</h1>
                                        <p class="text-white" style="font-size:36px"><em>Juntos logramos tus sueños</em></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <br>
                                        <a href="http://www.upeu.edu.pe/" class="btn btn-gray-transparent btn-animated">Ver Detalles<i class="pl-10 fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

            <div w3-include-html="footer.html"></div>
            <script>
                                w3IncludeHTML();
            </script>
            <!-- footer end -->

        </div>

    </div>
    <!-- footer top end -->		
    <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
    <!-- ================ -->


    <!-- .footer start -->
    <!-- ================ -->


    <!-- footer end -->

    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- jQuery Revolution Slider  -->
    <script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Isotope javascript -->
    <script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Morphext -->
    <script type="text/javascript" src="plugins/morphext/morphext.min.js"></script>
    <!-- Google Maps javascript -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=your_google_map_key"></script>
    <script type="text/javascript" src="js/google.map.config.js"></script>
    <!-- Background Video -->
    <script src="plugins/vide/jquery.vide.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Pace javascript -->
    <script type="text/javascript" src="plugins/pace/pace.min.js"></script>
    <!-- SmoothScroll javascript -->
    <script type="text/javascript" src="plugins/jquery.browser.js"></script>
    <script type="text/javascript" src="plugins/SmoothScroll.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>

    <script src="plugins/bootstrap-datetimepicker.min.js"></script>
    <script src="plugins/bootstrap-datetimepicker.es.js"></script>

</body>
<script>

                                $(function () {

                                    $('#person_fechin').datetimepicker(
                                            {
                                                format: "yyyy-mm-dd",
                                                language: 'es',
                                                weekStart: 1,
                                                todayBtn: 1,
                                                autoclose: 1,
                                                todayHighlight: 1,
                                                startView: 2,
                                                minView: 2,
                                                forceParse: 0
                                            }
                                    );
                                    $('#person_fechout').datetimepicker(
                                            {
                                                format: "yyyy-mm-dd",
                                                language: 'es',
                                                weekStart: 1,
                                                todayBtn: 1,
                                                autoclose: 1,
                                                todayHighlight: 1,
                                                startView: 2,
                                                minView: 2,
                                                forceParse: 0
                                            }
                                    );
                                });
</script>

<script type="text/javascript">

    jQuery(document).ready(function () {
        $(".oculto").hide();
        $(".btn").click(function () {
            var nodo = $(this).attr("href");

            if ($(nodo).is(":visible")) {
                $(nodo).hide();
                return false;
            } else {
                $(".oculto").hide("slow");
                $(nodo).fadeToggle("fast");
                return false;
            }
        });
    });

    jQuery(document).ready(function () {
        $("input[type=radio]").click(function (event) {
            var valor = $(event.target).val();
            if (valor == "Factura") {
                $("#div1").show();
                $("#div2").hide();
                
                $('#razonsoc2').val("");
            } else if (valor == "Boleta") {
                $("#div1").hide();
                $("#div2").show();
                
                $('#razonsoc').val("");
                $('#ruc').val("");
                $('#direccion').val("");
            } else {
                //nada
            }
        });

    });
</script>
</html>
