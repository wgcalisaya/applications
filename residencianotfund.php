<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="en">
	<!--<![endif]-->
    <script src="js/include.js"></script>	

	<head>
		<meta charset="utf-8">
		<title>Escuela de Posgrado | UPeU</title>
		<meta name="description" content="Profesionales alatamente confiables">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="plugins/hover/hover-min.css" rel="stylesheet">		
		<link href="plugins/morphext/morphext.css" rel="stylesheet">
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="css/skins/gold.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="css/custom.css" rel="stylesheet">
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<!-- "gradient-background-header": applies gradient background to header -->
	<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
	<body class="no-trans front-page   page-loader-2">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				

				<!-- ================ --> 
			<div w3-include-html="header.html"></div>

            <script>
                w3IncludeHTML();
            </script>
				<!-- header end -->
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner banner-big-height dark-translucent-bg padding-bottom-clear" style="background-image:url('images/hotel-banner.jpg');background-position: 50% 32%;">
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h1 class="title">Residencias UPeU</h1>
							<div class="separator mt-10"></div>
						</div>
					</div>
				</div>
				<!-- section start -->
				<!-- ================ -->
				<div class="dark-translucent-bg section">
					<div class="container">
						<!-- filters start -->
						<div class="sorting-filters text-center mb-20">
                        <?php 
error_reporting(0); 
$nombre = $_POST['nombre']; 
$email= $_POST['email']; 
$procedencia = $_POST['procedencia']; 
$unidad = $_POST['unidad']; 
$habitacion = $_POST['habitacion']; 
$uso = $_POST['uso']; 
$ingreso = $_POST['ingreso']; 
$horain = $_POST['horain']; 
$salida = $_POST['salida']; 
$horaout = $_POST['horaout']; 
$header = 'From: ' . $mail . ", procedente de ".$procedencia."\r\n"; 
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n"; 
$header .= "Mime-Version: 1.0 \r\n"; 
$header .= "Content-Type: text/plain"; 

$mensaje = "Este mensaje fue enviado por " . $nombre . " \r\n"; 
$mensaje .= "Su e-mail es: " . $email . " \r\n"; 
$mensaje .= "Unidad de Posgrado: " . $unidad . " \r\n";
$mensaje .= "Tipo de habitación: " . $habitacion . " \r\n";
$mensaje .= "Condisión de uso" . $_POST['uso'] . " \r\n"; 
$mensaje .= "Fecha de ingreso " . $_POST['ingreso'] . " \r\n"; 
$mensaje .= "Hora de ingreso " . $_POST['horain'] . " \r\n"; 
$mensaje .= "Fecha de salida " . $_POST['salida'] . " \r\n"; 
$mensaje .= "Hora de salida " . $_POST['horaout'] . " \r\n"; 


$para = 'soporteposgrado@upeu.edu.pe'; 
$asunto = 'RESERVACIÓN DE HABITACIONES'; 

mail($para, $asunto, utf8_decode($mensaje), $header); 

echo 'Gracias, pronto nos pondremos en contacto con Usted'; 

?> 
                        

						</div>
						<!-- filters end -->
					</div>
				</div>
				<!-- section end -->
		
                <div class="col-md-8 text-center col-md-offset-2 pv-20">
                <p>* Normal: Compartimiento para 2 personas.</p>
                <p>* Vip: Compartimiento para 4 personas</p>
                </div>
                  <div class="col-md-8 text-center col-md-offset-2 pv-20">
             
                <p>** Si es INDIVIDUAL con habitación normal, se asume el costo total de las 2 personas.</p>
                <p>** Si es INDIVIDUAL con habitación vip, se asume el costo total de las 4 personas.</p>
                </div>
			</div>
			<!-- banner end -->

			<!-- section start -->
			<!-- ================ --><!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
           <section class="section clearfix">
				<div class="container">
            <div class="tab-content clear-style">
						<div class="tab-pane active" id="pill-1">
							<div class="row">
								<div class="col-md-3">
									<div class="image-box text-center style-2 mb-20">
										<img src="images/restaurant-dish-1.jpg" alt="" class="img-circle img-thumbnail">
										<div class="body padding-horizontal-clear">
											<h4 class="logo-font title">Residencia</h4>
											<p class="small mb-10">Cambio de ropa de cama semanal, papel higiénico, jaboncillo y champú por semana, wifi.</p>
											<p class="lead text-default">S/ 18.00</p>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="image-box text-center style-2 mb-20">
										<img src="images/restaurant-dish-2.jpg" alt="" class="img-circle img-thumbnail">
										<div class="body padding-horizontal-clear">
											<h4 class="logo-font title">Lavandería</h4>
											<p class="small mb-10">Lavado de 6 prendas por semana</p>
											<p class="lead text-default">S/ 3.42</p>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="image-box text-center style-2 mb-20">
										<img src="images/restaurant-dish-3.jpg" alt="" class="img-circle img-thumbnail">
										<div class="body padding-horizontal-clear">
											<h4 class="logo-font title">Alimentación</h4>
											<p class="small mb-10">Alimentación exclusiva del país, buffet.</p>
											<p class="lead text-default">S/ 26.58</p>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="image-box text-center style-2 mb-20">
										<img src="images/restaurant-dish-4.jpg" alt="" class="img-circle img-thumbnail">
										<div class="body padding-horizontal-clear">
											<h4 class="logo-font title">Costo total</h4>
											<p class="small mb-10">Incluye todos los servicios mencionados.</p>
											<p class="lead text-default">S/ 48.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                        </div>
                        
				
					</div>
                    </section>
			<section class="light-gray-bg pv-40">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="text-center">Servicios <strong>UPeU</strong></h2>
							<div class="separator"></div>
							<p class="large text-center">La residecia de la UPeU es un ambiente en donde, además de disfrutar de la cercanía de sus aulas, comedor, facultades y áreas administrativas, se encuentra con Dios y desarrolla un carácter puro, que refleje únicamente lo bueno, que es parte de la formación del estudiante que brinda la Universidad Peruana Unión <a href="http://www.upeu.edu.pe/residencia-senoritas/" target="new">Leer más..</a></p>
							<br>
						</div>
					</div>
					<div class="row grid-space-0">
						<div class="col-md-6">
							<div class="image-box text-center">
								<div class="overlay-container">
									<img src="images/servicios/61036650.jpg" alt="">
									<div class="overlay-top">
										<div class="text">
											<h3><a href="#">Residencia de Señoritas</a></h3>
									
										</div>
									</div>
									<div class="overlay-bottom">
										<div class="links">
											<!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="image-box text-center">
								<div class="overlay-container">
									<img src="images/servicios/96477360.jpg" alt="">
									<div class="overlay-top">
										<div class="text">
											<h3>Areas verdes</h3>
											<p class="small"></p>
										</div>
									</div>
									<div class="overlay-bottom">
										<div class="links">
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="image-box text-center">
								<div class="overlay-container">
									<img src="images/servicios/96477434.jpg" alt="">
									<div class="overlay-top">
										<div class="text">
											<h3>Comedor</h3>
											<p class="small"></p>
										</div>
									</div>
									<div class="overlay-bottom">
										<div class="links">
											<!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="image-box text-center">
								<div class="overlay-container">
									<img src="images/servicios/comedor.jpg" alt="">
									<div class="overlay-top">
										<div class="text">
											<h3>Servicios del comedor</h3>
											<p class="small"></p>
										</div>
									</div>
									<div class="overlay-bottom">
										<div class="links">
											<!--<a href="#" class="btn btn-gray-transparent btn-animated">View Details <i class="pl-10 fa fa-arrow-right"></i></a>-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</section>
			<!-- section end -->

			<!-- footer top start -->
			<!-- ================ -->
			<div class="dark-bg footer-top animated-text default-hovered">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
									<div class="col-sm-8">
										<h1 class="title">Bienvenidos a la UPeU</h1>
										<p class="text-white" style="font-size:36px"><em>Juntos logramos tus sueños</em></p>
									</div>
									<div class="col-sm-4">
										<br>
										<a href="http://www.upeu.edu.pe/" class="btn btn-gray-transparent btn-animated">Ver Detalles<i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                
                
                
                		
		</div>
        
        	<div w3-include-html="footer.html"></div>
            <script>
                w3IncludeHTML();
            </script>
            <!-- footer end -->

        </div>
                
			</div>
			<!-- footer top end -->		
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
		

				<!-- .footer start -->
				<!-- ================ -->
	

			<!-- footer end -->
	
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>
		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>
		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
		<!-- Morphext -->
		<script type="text/javascript" src="plugins/morphext/morphext.min.js"></script>
		<!-- Google Maps javascript -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=your_google_map_key"></script>
		<script type="text/javascript" src="js/google.map.config.js"></script>
		<!-- Background Video -->
		<script src="plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- Pace javascript -->
		<script type="text/javascript" src="plugins/pace/pace.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>
		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>
		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>

	</body>
</html>
