<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gu&iacute;a de evaluaci&oacute;n docente</title>
<link rel="stylesheet" type="text/css" href="ado.css">
<style type="text/css">
.mod_pagina .mod_titulo h5 {font-family: Arial, Verdana, Tahoma; text-align: center;}
.mod_pagina .intro {font-family: Arial, Verdana, Tahoma; text-align: justify;}
.mod_pagina .mod_titulo h4 {font-family: Arial, Verdana, Tahoma; text-align: center;}
.subtitulo {  
font-family: Arial, Verdana, Tahoma; 
font-weight: bold; 
font-size: 18px; 
}
.subtitulo1 {font-family: Arial, Verdana, Tahoma; 
font-weight: bold; 
font-size: 16px; 
}
</style>
</head>

<body>
<br />
<form action="almacenar.php" method="post" name="formu">
<input type="hidden" name="formulario" value="2" />
<input type="hidden" name="ciclo" value="I" />
<input type="hidden" name="ingreso" value="2013-II" />
<p align="center" class="subtitulo">EVALUACI&Oacute;N DE DOCENTES DE MAESTR&Iacute;A</p><hr color="#00CC33" />
<p align="center" class="subtitulo">&nbsp;</p>
      <table width="862" border="0" align="center">
        <tr>
          <td width="856" align="center">
            <p class="link_o">Instrucciones: El presente instrumento tiene por objetivo evaluar al docente en el desarrollo de actividades significativas. En el margen izquierdo se encuentran los criterios a evaluar y en el margen derecho la escala de puntuaci&oacute;n, la cual se interpreta de la siguiente manera: </p>
            <p class="link_o">&nbsp;</p>
            <table width="600" border="0" cellspacing="5" cellpadding="5">
              <tr>
                <td align="center" bgcolor="#99EC84"><span class="espe">5) Excelente</span></td>
                <td align="center" bgcolor="#99EC84"><span class="espe">4) Muy bueno</span></td>
                <td align="center" bgcolor="#99EC84"><span class="espe">3) Bueno</span></td>
                <td align="center" bgcolor="#99EC84"><span class="espe">2) Regular&nbsp;</span></td>
                <td align="center" bgcolor="#99EC84"><span class="espe">1) Malo</span></td>
              </tr>
            </table>
          <p class="espe">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
        </tr>
      </table>
      <table width="900" border="0" align="center">

        <tr>

          <td align="center"><span class="subtitulo1">Curso / Docente :</span></td>

        </tr>

        <tr>

          <td width="335" align="center"><label for="select"></label>

            <select name="select" id="select">
            
<!-- PROGRAMA DE MAESTRÍA -->

<option value="Maestr&iacute;a en Enfermer&iacute;a (Lima) / Bioestadística /Dr. Juan Soria Quijaite"> Maestr&iacute;a en Enfermer&iacute;a (Lima) /  Bioestad&iacute;stica/ Dr. Juan Soria Quijaite</option>

<option value="Maestr&iacute;a en Enfermer&iacute;a (Lima) / Planeamiento Estratégico / Dra. Isabel Chaw Ortega">Maestr&iacute;a en Enfermer&iacute;a (Lima) / Planeamiento Estrat&eacute;gico / Dra. Isabel Chaw Ortega</option>

<option value="Maestr&iacute;a en Enfermer&iacute;a (Lima) / Marketing Social / Mg. John Eloy Ponce Pardo">Maestr&iacute;a en Enfermer&iacute;a (Lima) /Marketing Social / Mg. John Eloy Ponce Pardo</option>

<option value="Maestr&iacute;a en Enfermer&iacute;a (Lima) / Seminario de Tesis II / Mg. Irene Zapata Silva">Maestr&iacute;a en Enfermer&iacute;a (Lima) / Seminario de Tesis II / Mg. Irene Zapata Silva</option>


          </select></td>

        </tr>

      </table>
      <p><br/>
        
      </p>
      <table width="800" border="0" align="center" cellspacing="0">
<tr class="espe" align="center">
                <th width="613"><div align="left"> Aspecto &eacute;tico social</div></th>
                <th width="35">5</th>
                <th width="35">4</th>
                <th width="35">3</th>
                <th width="35">2</th>
                <th width="35">1</th>
              </tr>
              <!--  1r level -->
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">&nbsp; 1. Utiliza un lenguaje culto </td>
                <!--  2n level -->
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(1)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(1)" value="4" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(1)" value="3" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(1)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(1)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" class="inf_N">&nbsp; 2. Asiste puntualmente a las sesiones programadas seg&uacute;n horario. </td>
                <!--  2n level -->
                <td width="35"><label>
                  <input type="radio" name="resultado(2)" value="5" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(2)" value="4" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(2)" value="3" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(2)" value="2" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(2)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">&nbsp; 3. Inspira confianza. </td>
                <!--  2n level -->
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(3)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(3)" value="4" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(3)" value="3" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(3)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(3)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" class="inf_N">&nbsp; 4. Demuestra preocupaci&oacute;n y comprensi&oacute;n por los problemas de los alumnos. </td>
                <!--  2n level -->
                <td width="35"><label>
                  <input type="radio" name="resultado(4)" value="5" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(4)" value="4" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(4)" value="3" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(4)" value="2" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(4)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 5. Realiza medidas correctivas personales en privado. </td>
                <!--  2n level -->
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(5)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(5)" value="4" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(5)" value="3" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(5)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(5)" value="1" />
                </label></td>
              </tr>
            </table>
    
            <p>&nbsp;</p>
            <table width="800" border="0" align="center" cellspacing="0">
              <tr class="espe" align="center">
                <th><div align="left">Desempe&ntilde;o profesional docente</div></th>
                <th width="35">5</th>
                <th width="35">4</th>
                <th width="35">3</th>
                <th width="35">2</th>
                <th width="35">1</th>
              </tr>
              <!--  1r level -->
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 1. Entrega oportunamente el s&iacute;labo y orienta el desarrollo del curso. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(6)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(6)" value="4" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(6)" value="3" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(6)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(6)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" class="inf_N">
                &nbsp; 2. Desarrolla la asignatura seg&uacute;n lo planificado. </td>
                <!--  2n level -->
                <td><label>
                  <input type="radio" name="resultado(7)" value="5" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(7)" value="4" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(7)" value="3" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(7)" value="2" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(7)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 3. Demuestra dominio de los contenidos. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(8)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(8)" value="4" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(8)" value="3" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(8)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(8)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" class="inf_N">
                &nbsp; 4. Utiliza metodolog&iacute;as y herramientas colaborativas que favorecen el aprendizaje significativo y participativo en la sesi&oacute;n. </td>
                <!--  2n level -->
                <td><label>
                  <input type="radio" name="resultado(9)" value="5" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(9)" value="4" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(9)" value="3" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(9)" value="2" />
                </label></td>
                <td width="35"><label>
                  <input type="radio" name="resultado(9)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 5. Fomenta el desarrollo del pensamiento cr&iacute;tico y creativo. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(10)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(10)" value="4" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(10)" value="3" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(10)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(10)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#FFFFFF" class="inf_N">
                &nbsp; 6. Promueve el trabajo en equipo. </td>
                <!--  2n level -->
                <td bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(12)" value="5" />
                </label></td>
                <td bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(12)" value="4" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(12)" value="3" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(12)" value="2" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(12)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 7. Utiliza adecuados recursos did&aacute;cticos: letras, colores, etc. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(13)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(13)" value="4" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(13)" value="3" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(13)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(13)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#FFFFFF" class="inf_N">
                &nbsp; 8. Realiza evaluaciones coherentes con las capacidades formuladas y contenidos desarrollados. </td>
                <!--  2n level -->
                <td bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(14)" value="5" />
                </label></td>
                <td bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(14)" value="4" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(14)" value="3" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(14)" value="2" />
                </label></td>
                <td width="35" bgcolor="#FFFFFF"><label>
                  <input type="radio" name="resultado(14)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">&nbsp; 9. Entrega resultados de ex&aacute;menes y trabajos revisados en la fecha programada. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(15)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(15)" value="4" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(15)" value="3" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(15)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(15)" value="1" />
                </label></td>
              </tr>
  </table>
        
<p>&nbsp;</p>
            <table width="800" border="0" align="center" cellspacing="0">
              <tr class="espe" align="center">
                <th width="613"><div align="left">Aspecto espiritual</div></th>
                <th width="35">5</th>
                <th width="35">4</th>
                <th width="35">3</th>
                <th width="35">2</th>
                <th width="35">1</th>
              </tr>
              <!--  1r level -->
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 1. Realiza reflexiones espirituales antes del desarrollo de las clases</td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(16)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(16)" value="4" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(16)" value="3" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(16)" value="2" />
                </label></td>
                <td width="35" bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(16)" value="1" />
                </label></td>
              </tr>
              <tr class="par" align="center">
                <td align="left" class="inf_N">
                &nbsp; 2. Promueve los valores cristianos. </td>
                <!--  2n level -->
                <td><label>
                  <input type="radio" name="resultado(17)" value="5" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(17)" value="4" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(17)" value="3" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(17)" value="2" />
                </label></td>
                <td><label>
                  <input type="radio" name="resultado(17)" value="1" />
                </label></td>
              </tr>
              <tr class="impar" align="center">
                <td align="left" bgcolor="#99EC84" class="inf_N">
                &nbsp; 3. Act&uacute;a de acuerdo a los principios &eacute;ticos - cristianos. </td>
                <!--  2n level -->
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(18)" value="5" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(18)" value="4" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(18)" value="3" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(18)" value="2" />
                </label></td>
                <td bgcolor="#99EC84"><label>
                  <input type="radio" name="resultado(18)" value="1" />
                </label></td>
              </tr>
            </table>
            <br />
            <br />
            
            <table width="800" border="0" align="center" cellspacing="0">
                <tr class="espe" align="center">
                  <th width="613"><div align="left">Observaciones</div></th>
                  <th width="35">&nbsp;</th>
                  <th width="35">&nbsp;</th>
                  <th width="35">&nbsp;</th>
                  <th width="35">&nbsp;</th>
                  <th width="35">&nbsp;</th>
                </tr>
                <!--  1r level -->
                <tr class="impar" align="center">
                  <td align="left" bgcolor="#99EC84" class="inf_N" colspan="6">
                  <textarea name="recomendaciones" cols="98" rows="5"></textarea></td>
                </tr>
              </table>
       
 &nbsp; 
 <p>&nbsp;  </p>
 <p align="center">
   <input name="envio" type="submit" class="intro"  value="Enviar datos" />
 </p>
</form>
</body>
</html>